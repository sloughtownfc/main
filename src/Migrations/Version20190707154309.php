<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190707154309 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE competitions CHANGE bench_count bench_count INT DEFAULT 5 NOT NULL');
        $this->addSql('ALTER TABLE fixtures CHANGE report_home_score report_home_score INT DEFAULT NULL, CHANGE report_away_score report_away_score INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fixtures ADD CONSTRAINT FK_5CB9E5349ED3936B FOREIGN KEY (report_mom) REFERENCES players (id)');
        $this->addSql('CREATE INDEX IDX_5CB9E5349ED3936B ON fixtures (report_mom)');
        $this->addSql('DROP INDEX model_id ON gallery_images_tags');
        $this->addSql('ALTER TABLE gallery_images_tags RENAME INDEX fk_gallery_images_tags_tag_id_idx TO IDX_BCBEEC1DBAD26311');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY fk_messages_contact_id');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT FK_DB021E96E7A1254A FOREIGN KEY (contact_id) REFERENCES contacts (id)');
        $this->addSql('ALTER TABLE players CHANGE position_id position_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stats CHANGE player_id player_id INT DEFAULT NULL, CHANGE fixture_id fixture_id INT DEFAULT NULL, CHANGE shirt shirt INT DEFAULT 0 NOT NULL, CHANGE sub_shirt sub_shirt INT DEFAULT 0 NOT NULL, CHANGE goals goals INT DEFAULT 0 NOT NULL, CHANGE bookings bookings INT DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE roles_users RENAME INDEX role_id_idx TO IDX_3D80FB2CD60322AC');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE competitions CHANGE bench_count bench_count TINYINT(1) DEFAULT \'5\' NOT NULL');
        $this->addSql('ALTER TABLE fixtures DROP FOREIGN KEY FK_5CB9E5349ED3936B');
        $this->addSql('DROP INDEX IDX_5CB9E5349ED3936B ON fixtures');
        $this->addSql('ALTER TABLE fixtures CHANGE report_home_score report_home_score TINYINT(1) DEFAULT NULL, CHANGE report_away_score report_away_score TINYINT(1) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX model_id ON gallery_images_tags (gallery_image_id, tag_id)');
        $this->addSql('ALTER TABLE gallery_images_tags RENAME INDEX idx_bcbeec1dbad26311 TO fk_gallery_images_tags_tag_id_idx');
        $this->addSql('ALTER TABLE messages DROP FOREIGN KEY FK_DB021E96E7A1254A');
        $this->addSql('ALTER TABLE messages ADD CONSTRAINT fk_messages_contact_id FOREIGN KEY (contact_id) REFERENCES contacts (id) ON UPDATE NO ACTION ON DELETE SET NULL');
        $this->addSql('ALTER TABLE players CHANGE position_id position_id INT NOT NULL');
        $this->addSql('ALTER TABLE roles_users RENAME INDEX idx_3d80fb2cd60322ac TO role_id_idx');
        $this->addSql('ALTER TABLE stats CHANGE fixture_id fixture_id INT NOT NULL, CHANGE player_id player_id INT NOT NULL, CHANGE shirt shirt INT NOT NULL, CHANGE sub_shirt sub_shirt INT NOT NULL, CHANGE goals goals INT NOT NULL, CHANGE bookings bookings INT NOT NULL');
    }
}
