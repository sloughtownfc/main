<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200901235618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM league_entries');
        $this->addSql('ALTER TABLE league_entries MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE league_entries DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE league_entries DROP id, CHANGE team_id team_id INT NOT NULL');
        $this->addSql('ALTER TABLE league_entries ADD PRIMARY KEY (team_id, `type`, `position`)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE league_entries DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE league_entries ADD id INT AUTO_INCREMENT NOT NULL, CHANGE team_id team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE league_entries ADD PRIMARY KEY (id)');
    }
}
