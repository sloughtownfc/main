<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190711231509 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE INDEX idx_oppositions_autocomplete ON oppositions (name)');
        $this->addSql('ALTER TABLE players ADD full_name VARCHAR(100) NOT NULL');
        $this->addSql('CREATE INDEX idx_players_autocomplete ON players (full_name)');
        
        $this->addSql('UPDATE players SET full_name = CONCAT(first_name, \' \' , last_name)');
        $this->addSql('UPDATE players SET full_name = last_name WHERE first_name = \'Unknown\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX idx_oppositions_autocomplete ON oppositions');
        $this->addSql('DROP INDEX idx_players_autocomplete ON players');
        $this->addSql('ALTER TABLE players DROP full_name');
    }
}
