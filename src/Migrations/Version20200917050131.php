<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200917050131 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fixtures ADD tickets_url VARCHAR(255) DEFAULT NULL, ADD live_stream_id VARCHAR(64) DEFAULT NULL, DROP away_alternate_kit');
        $this->addSql('ALTER TABLE oppositions DROP colour_away_background, DROP colour_away_foreground, DROP colour_home_background, DROP colour_home_foreground');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fixtures ADD away_alternate_kit TINYINT(1) DEFAULT NULL, DROP tickets_url, DROP live_stream_id');
        $this->addSql('ALTER TABLE oppositions ADD colour_away_background VARCHAR(7) NOT NULL COLLATE utf8_general_ci, ADD colour_away_foreground VARCHAR(7) NOT NULL COLLATE utf8_general_ci, ADD colour_home_background VARCHAR(7) NOT NULL COLLATE utf8_general_ci, ADD colour_home_foreground VARCHAR(7) NOT NULL COLLATE utf8_general_ci');
    }
}
