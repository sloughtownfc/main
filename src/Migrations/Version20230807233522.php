<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230807233522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE messages RENAME INDEX fk_db021e96e7a1254a TO fk_messages_contact_id_idx');
        $this->addSql('ALTER TABLE players ADD on_loan TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE league_entries DROP FOREIGN KEY FK_9451AAB4296CD8AE');
        $this->addSql('ALTER TABLE messages RENAME INDEX fk_messages_contact_id_idx TO FK_DB021E96E7A1254A');
        $this->addSql('ALTER TABLE players DROP on_loan');
    }
}
