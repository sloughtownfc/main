<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200802174206 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE teams (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(64) NOT NULL, url VARCHAR(32) NOT NULL, active TINYINT(1) NOT NULL, UNIQUE INDEX url (url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE competitions ADD team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE competitions ADD CONSTRAINT FK_A7DD463D296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('CREATE INDEX IDX_A7DD463D296CD8AE ON competitions (team_id)');
        $this->addSql('ALTER TABLE fixtures ADD team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fixtures ADD CONSTRAINT FK_5CB9E534296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('CREATE INDEX IDX_5CB9E534296CD8AE ON fixtures (team_id)');
        $this->addSql('ALTER TABLE oppositions ADD team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE oppositions ADD CONSTRAINT FK_6E74A758296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('CREATE INDEX IDX_6E74A758296CD8AE ON oppositions (team_id)');
        $this->addSql('ALTER TABLE players ADD team_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE players ADD CONSTRAINT FK_264E43A6296CD8AE FOREIGN KEY (team_id) REFERENCES teams (id)');
        $this->addSql('CREATE INDEX IDX_264E43A6296CD8AE ON players (team_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE competitions DROP FOREIGN KEY FK_A7DD463D296CD8AE');
        $this->addSql('ALTER TABLE fixtures DROP FOREIGN KEY FK_5CB9E534296CD8AE');
        $this->addSql('ALTER TABLE oppositions DROP FOREIGN KEY FK_6E74A758296CD8AE');
        $this->addSql('ALTER TABLE players DROP FOREIGN KEY FK_264E43A6296CD8AE');
        $this->addSql('DROP TABLE teams');
        $this->addSql('DROP INDEX IDX_A7DD463D296CD8AE ON competitions');
        $this->addSql('ALTER TABLE competitions DROP team_id');
        $this->addSql('DROP INDEX IDX_5CB9E534296CD8AE ON fixtures');
        $this->addSql('ALTER TABLE fixtures DROP team_id');
        $this->addSql('DROP INDEX IDX_6E74A758296CD8AE ON oppositions');
        $this->addSql('ALTER TABLE oppositions DROP team_id');
        $this->addSql('DROP INDEX IDX_264E43A6296CD8AE ON players');
        $this->addSql('ALTER TABLE players DROP team_id');
    }
}
