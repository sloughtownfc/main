<?php

namespace App\Dto;

class TeamDto {
    public $name;
    public $url;
    public $shortName;
    public $tableName;
}
