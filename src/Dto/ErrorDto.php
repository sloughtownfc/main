<?php

namespace App\Dto;

class ErrorDto {
    private $status;
    private $title;
    private $detail;
    
    public function __construct(int $status, string $title, string $detail) {
        $this->status = $status;
        $this->title = $title;
        $this->detail = $detail;
    }
    
    public function getStatus(): int
    {
        return $this->status;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
    
    public function getDetail(): string
    {
        return $this->detail;
    }
}
