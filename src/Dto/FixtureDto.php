<?php

namespace App\Dto;

class FixtureDto {
    public $matchDate;
    public $kickoff;
    public $isHome;
    public $matchSponsor;
    public $ballSponsor;
    public $ticketUrl;
    public $videoUrl;
    public $videoPublished;
    public $reportHomeScore;
    public $reportAwayScore;
    public $reportUrl;
    public $reportPublished;
    public $liveStreamUrl;
    public $team;
    public $opposition;
    public $competition;
}
