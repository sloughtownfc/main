<?php

namespace App\Dto;

class LeagueEntryDto {
    public $position;
    public $name;
    public $played;
    public $won;
    public $drawn;
    public $lost;
    public $for;
    public $against;
    public $goalDifference;
    public $points;
}
