<?php

namespace App\Dto;

class OppositionDto {
    public $name;
    public $logo;
    public $groundsVenue;
}
