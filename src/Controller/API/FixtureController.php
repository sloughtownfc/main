<?php

namespace App\Controller\API;

use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Dto\ErrorDto;
use App\Dto\FixtureDto;
use App\Entity\Fixture;
use App\Entity\Team;

/**
 * @Route("api/fixtures", name="fixtures_")
 */
class FixtureController extends APIController
{
    private $mapper;
    
    public function __construct(AutoMapperInterface $mapper) {
        $this->mapper = $mapper;
    }
    
    /**
     * @Route("", name="list")
     */
    public function list(Request $request)
    {   
        $teamUrl = $request->query->get('teamUrl');
        $direction = $request->query->get('direction') ?? 'next';
        $count = $request->query->get('count') ?? 1;
        
        $team = null;
        
        if ($teamUrl) {
            $team = $this->getDoctrine()->getRepository(Team::class)->findOneBy(['url' => $teamUrl]);
            
            if (!$team) {
                return $this->badRequest(new ErrorDto(400, 'invalid_team', 'Invalid teamUrl parameter supplied'));
            }
        }
        
        $fixtures = $this->getDoctrine()->getRepository(Fixture::class)->listByTeamAndDirection($team, $direction, $count);
        
        return $this->json([
            'fixtures' => $this->mapper->mapMultiple($fixtures, FixtureDto::class),
            'direction' => $direction,
            'count' => $count
        ]);
    }
}
