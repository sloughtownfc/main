<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract class APIController extends AbstractController {
    protected function badRequest($data): JsonResponse
    {
        return $this->json($data, $status = 400);
    }
    
    protected function noContent(): Response
    {
        $response = new Response();
        $response->setStatusCode(204);
        
        return $response;
    }
    
    protected function notFound($data): JsonResponse
    {
        return $this->json($data, $status = 404);
    }
    
    protected function success($data): JsonResponse
    {
        return $this->json($data);
    }
}
