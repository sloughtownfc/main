<?php

namespace App\Controller\API;

use AutoMapperPlus\AutoMapperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Dto\ErrorDto;
use App\Dto\LeagueEntryDto;
use App\Dto\TeamDto;
use App\Entity\LeagueEntry;
use App\Entity\Team;

/**
 * @Route("api/tables", name="tables_")
 */
class TableController extends APIController
{
    private $mapper;
    
    public function __construct(AutoMapperInterface $mapper) {
        $this->mapper = $mapper;
    }
    
    /**
     * @Route("/league", name="league")
     */
    public function league(Request $request)
    {   
        $count = $request->query->get('count');
        if ($count != null && $count % 2 == 0) {
            return $this->badRequest(new ErrorDto(400, 'invalid_count', 'Invalid count parameter supplied; must be an odd number and greater than 0.'));
        }
        
        $teamUrl = $request->query->get('teamUrl') ?? 'mens';
        $team = $this->getDoctrine()->getRepository(Team::class)->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            return $this->badRequest(new ErrorDto(400, 'invalid_team', 'Invalid teamUrl parameter supplied'));
        }
        
        $type = $request->query->get('type') ?? 'full';
        $titleAffix = '';
        
        switch($type) {
            case 'full':
                break;
            case 'home':
                $titleAffix = '- Home Table';
                break;
            case 'away':
                $titleAffix = '- Away Table';
                break;
            case 'goals':
                $titleAffix = '- Goals Scored';
                break;
            case 'conceded':
                $titleAffix = '- Goals Conceded';
                break;
            default:
                return $this->badRequest(new ErrorDto(400, 'invalid_type', 'Invalid type parameter supplied'));
        }

        $entries = $count == null ?
                $this->getDoctrine()->getRepository(LeagueEntry::class)->findBy(['team' => $team, 'type' => $type], ['position' => 'ASC']) :
                $this->getDoctrine()->getRepository(LeagueEntry::class)->getSnapshot($team, $type, $count);
        
        return $this->success([
            'table' => $this->mapper->mapMultiple($entries, LeagueEntryDto::class),
            'team' => $this->mapper->map($team, TeamDto::class),
            'titleAffix' => $titleAffix,
            'teamUrl' => $teamUrl,
            'type' => $type,
            'count' => $count ?? count($entries)
        ]);
    }
}
