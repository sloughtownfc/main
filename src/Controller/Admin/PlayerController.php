<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Player;
use App\Entity\Team;
use App\Form\PlayerType;

/**
 * @Route("{teamUrl}/player", name="player_")
 */
class PlayerController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index(Request $request, $teamUrl, $page = 1, $count = 25)
    {
        $query = $request->query->get('query');
        $playerRepo = $this->getDoctrine()->getRepository(Player::class);
        
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        return $this->render('admin/player/index.html.twig', [
            'playerItems' => $playerRepo->findAllByTeamAndPage($team, $page, $count, $query),
            'totalItems' => $playerRepo->getCount($team, $query),
            'currentPage' => $page,
            'pageSize' => $count,
            'query' => $query,
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request, $teamUrl)
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        $player = new Player();
        $player->addTeam($team);
        
        $form = $this->createForm(PlayerType::class, $player);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $player = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($player);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Player!');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/player/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $playerRepo = $em->getRepository(Player::class);
        $player = $playerRepo->findOneById($id);
        
        if (!$player) {
            $this->addFlash('warning', 'Player not found.');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }    
        
        if (count($player->getStats()) > 0) {
            $this->addFlash('warning', 'Player has one or more Stats associated with it.');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        if (count($player->getTeams()) > 0) {
            $this->addFlash('warning', 'Player has one or more Teams associated with it.');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        $em->remove($player);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Player!');
        
        return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $playerRepo = $em->getRepository(Player::class);
        $player = $playerRepo->findOneById($id);
        
        if (!$player) {
            $this->addFlash('warning', 'Player not found.');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        $form = $this->createForm(PlayerType::class, $player);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $player = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Player!');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/player/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
    
    /**
     * @Route("/view/{id}", name="view", requirements={"id"="\d+"})
     */
    public function view($teamUrl, $id)
    {
        $playerRepo = $this->getDoctrine()->getRepository(Player::class);
        $player = $playerRepo->getPlayerAndPositionAndStatsAndFixturesAndOppositionAndCompetitionById($id);
        
        if (!$player) {
            $this->addFlash('warning', 'Player not found.');
            
            return $this->redirectToRoute('admin_player_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/player/view.html.twig', [
            'player' => $player,
            'teamUrl' => $teamUrl
        ]);
    }
}
