<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Season;
use App\Form\SeasonType;

/**
 * @Route("season", name="season_")
 */
class SeasonController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($page = 1, $count = 25)
    {
        $seasonRepo = $this->getDoctrine()->getRepository(Season::class);
        
        return $this->render('admin/season/index.html.twig', [
            'seasonItems' => $seasonRepo->findAllByPage($page, $count),
            'totalItems' => $seasonRepo->getCount(),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request)
    {
        $season = new Season();
        
        $form = $this->createForm(SeasonType::class, $season);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $season = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($season);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Season!');
            return $this->redirectToRoute('admin_season_index');
        }
        
        return $this->render('admin/season/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $seasonRepo = $em->getRepository(Season::class);
        $season = $seasonRepo->findOneById($id);
        
        if (!$season) {
            $this->addFlash('warning', 'Season not found.');
            return $this->redirectToRoute('admin_season_index');
        }

        $em->remove($season);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Season!');
        return $this->redirectToRoute('admin_season_index');
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $seasonRepo = $em->getRepository(Season::class);
        $season = $seasonRepo->findOneById($id);
        
        if (!$season) {
            $this->addFlash('warning', 'Season not found.');
            return $this->redirectToRoute('admin_season_index');
        }

        $form = $this->createForm(SeasonType::class, $season);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $season = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Season!');
            return $this->redirectToRoute('admin_season_index');
        }
        
        return $this->render('admin/season/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
}
