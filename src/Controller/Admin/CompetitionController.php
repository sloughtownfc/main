<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Competition;
use App\Entity\Team;
use App\Form\CompetitionType;

/**
 * @Route("{teamUrl}/competition", name="competition_")
 */
class CompetitionController extends AbstractController
{   
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($teamUrl, $page = 1, $count = 25)
    {
        $competitionRepo = $this->getDoctrine()->getRepository(Competition::class);
        
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        return $this->render('admin/competition/index.html.twig', [
            'competitionItems' => $competitionRepo->findAllByTeamAndPage($team, $page, $count),
            'totalItems' => $competitionRepo->getCount($team),
            'currentPage' => $page,
            'pageSize' => $count,
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request, $teamUrl)
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        $competition = new Competition();
        $competition->setTeam($team);
        
        $form = $this->createForm(CompetitionType::class, $competition);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $competition = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($competition);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Competition!');
                        
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/competition/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $competitionRepo = $em->getRepository(Competition::class);
        $competition = $competitionRepo->findOneById($id);
        
        if (!$competition) {
            $this->addFlash('warning', 'Competition not found.');
                        
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        if (count($competition->getFixtures()) > 0) {
            $this->addFlash('warning', 'Competition has one or more Fixtures associated with it.');
                        
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        $em->remove($competition);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Competition!');
                    
        return $this->redirectToRoute('admin_competition_index', [
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $competitionRepo = $em->getRepository(Competition::class);
        $competition = $competitionRepo->findOneById($id);
        
        if (!$competition) {
            $this->addFlash('warning', 'Competition not found.');
            
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        $form = $this->createForm(CompetitionType::class, $competition);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $competition = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Competition!');
                        
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/competition/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
    
    /**
     * @Route("/view/{id}", name="view", requirements={"id"="\d+"})
     */
    public function view($teamUrl, $id)
    {
        $competitionRepo = $this->getDoctrine()->getRepository(Competition::class);
        $competition = $competitionRepo->getCompetitionAndFixturesById($id);
        
        if (!$competition) {
            $this->addFlash('warning', 'Competition not found.');
                        
            return $this->redirectToRoute('admin_competition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/competition/view.html.twig', [
            'competition' => $competition,
            'teamUrl' => $teamUrl
        ]);
    }
}
