<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Opposition;
use App\Entity\Team;
use App\Form\OppositionType;

/**
 * @Route("{teamUrl}/opposition", name="opposition_")
 */
class OppositionController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($teamUrl, $page = 1, $count = 25)
    {
        $oppositionRepo = $this->getDoctrine()->getRepository(Opposition::class);
        
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        return $this->render('admin/opposition/index.html.twig', [
            'oppositionItems' => $oppositionRepo->findAllByTeamAndPage($team, $page, $count),
            'totalItems' => $oppositionRepo->getCount($team),
            'currentPage' => $page,
            'pageSize' => $count,
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request, $teamUrl)
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        $opposition = new Opposition();
        $opposition->setTeam($team);
        
        $form = $this->createForm(OppositionType::class, $opposition);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $opposition = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($opposition);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Opposition!');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/opposition/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $oppositionRepo = $em->getRepository(Opposition::class);
        $opposition = $oppositionRepo->findOneById($id);
        
        if (!$opposition) {
            $this->addFlash('warning', 'Opposition not found.');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        if (count($opposition->getFixtures()) > 0) {
            $this->addFlash('warning', 'Opposition has one or more Fixtures associated with it.');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        $em->remove($opposition);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Opposition!');
            
        return $this->redirectToRoute('admin_opposition_index', [
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $oppositionRepo = $em->getRepository(Opposition::class);
        $opposition = $oppositionRepo->findOneById($id);
        
        if (!$opposition) {
            $this->addFlash('warning', 'Opposition not found.');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        $form = $this->createForm(OppositionType::class, $opposition);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $opposition = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Opposition!');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/opposition/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
    
    /**
     * @Route("/view/{id}", name="view", requirements={"id"="\d+"})
     */
    public function view($teamUrl, $id)
    {
        $oppositionRepo = $this->getDoctrine()->getRepository(Opposition::class);
        $opposition = $oppositionRepo->getOppositionAndFixturesById($id);
        
        if (!$opposition) {
            $this->addFlash('warning', 'Opposition not found.');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/opposition/view.html.twig', [
            'opposition' => $opposition,
            'teamUrl' => $teamUrl
        ]);
    }
}
