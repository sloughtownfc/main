<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Notification;
use App\Form\NotificationType;

/**
 * @Route("notification", name="notification_")
 */
class NotificationController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($page = 1, $count = 25)
    {
        $notificationRepo = $this->getDoctrine()->getRepository(Notification::class);
        
        return $this->render('admin/notification/index.html.twig', [
            'notificationItems' => $notificationRepo->findAllByPage($page, $count),
            'totalItems' => $notificationRepo->getCount(),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request)
    {
        $notification = new Notification();
        
        $form = $this->createForm(NotificationType::class, $notification);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $notification = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($notification);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Notification!');
            return $this->redirectToRoute('admin_notification_index');
        }
        
        return $this->render('admin/notification/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $notification = $em->getRepository(Notification::class)->findOneById($id);
        
        if (!$notification) {
            $this->addFlash('warning', 'Notification not found.');
            return $this->redirectToRoute('admin_notification_index');
        }

        $em->remove($notification);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Notification!');
        return $this->redirectToRoute('admin_notification_index');
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $notification = $em->getRepository(Notification::class)->findOneById($id);
        
        if (!$notification) {
            $this->addFlash('warning', 'Notification not found.');
            return $this->redirectToRoute('admin_notification_index');
        }

        $form = $this->createForm(NotificationType::class, $notification);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $notification = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Notification!');
            return $this->redirectToRoute('admin_notification_index');
        }
        
        return $this->render('admin/notification/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
}
