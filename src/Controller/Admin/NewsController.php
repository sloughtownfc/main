<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\News;
use App\Form\NewsType;

/**
 * @Route("news", name="news_")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($page = 1, $count = 25)
    {
        $newsItemRepo = $this->getDoctrine()->getRepository(News::class);
        
        return $this->render('admin/news/index.html.twig', [
            'newsItems' => $newsItemRepo->findAllByPage($page, $count),
            'totalItems' => $newsItemRepo->getCount(),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request)
    {
        $newsItem = new News();
        
        $form = $this->createForm(NewsType::class, $newsItem);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $newsItem = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($newsItem);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new News!');
            return $this->redirectToRoute('admin_news_index');
        }
        
        return $this->render('admin/news/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $newsItemRepo = $em->getRepository(News::class);
        $newsItem = $newsItemRepo->findOneById($id);
        
        if (!$newsItem) {
            $this->addFlash('warning', 'News not found.');
            return $this->redirectToRoute('admin_news_index');
        }

        $em->remove($newsItem);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed News!');
        return $this->redirectToRoute('admin_news_index');
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $newsItemRepo = $em->getRepository(News::class);
        $newsItem = $newsItemRepo->findOneById($id);
        
        if (!$newsItem) {
            $this->addFlash('warning', 'News not found.');
            return $this->redirectToRoute('admin_news_index');
        }

        $form = $this->createForm(NewsType::class, $newsItem);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $newsItem = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated News!');
            return $this->redirectToRoute('admin_news_index');
        }
        
        return $this->render('admin/news/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
}
