<?php

namespace App\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;
use App\Entity\Player;

/**
 * @Route(name="default_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index()
    {
        $notifications = [];
        
        $nextFixture = $this->getDoctrine()->getRepository(Fixture::class)->getNext();
        if ($nextFixture) {
            $today = \DateTime::createFromFormat('!Y-m-d', date('Y-m-d'));
            if ($today->diff($nextFixture->getMatchDate())->days == 0) {
                $title = 'Match day';
            }
            else {
                $title = 'Next Match';
            }
            
            $date = $nextFixture->getMatchDate()->format('Y-m-d');
            $homeOrAway = $nextFixture->getIsHome() != null ? $nextFixture->getIsHome() ? 'H' : 'A' : '?';
            $opposition = $nextFixture->getOpposition()->getName();
            $kickoff = $nextFixture->getKickoff() != null ? $nextFixture->getKickoff()->format('H:i') : 'N/A';
            
            $notifications[] = [
              'title' => $title,
              'message' => "$date: $homeOrAway v $opposition - Kick Off: $kickoff"
            ];
        }
        
        $playerNotifications = $this->getDoctrine()->getRepository(Player::class)->getPlayerNotifications();
        foreach ($playerNotifications as $playerNotification) {
            $notifications[] = [
              'title' => "{$playerNotification['first_name']} {$playerNotification['last_name']}",
              'message' => "is due to make his {$playerNotification['next_appearance']}th appearance with his next appearance."
            ];
        }
        
        return $this->render('admin/default/index.html.twig', [
            'controller_name' => 'DefaultController',
            'notifications' => $notifications
        ]);
    }
}
