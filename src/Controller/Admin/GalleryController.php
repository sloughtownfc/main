<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\GalleryImage;
use App\Entity\Tag;
use App\Form\ImageUploadType;
use App\Service\ImageService;

/**
 * @Route("gallery", name="gallery_")
 */
class GalleryController extends AbstractController
{
    /**
     * @Route("/list/{page}/{count}", name="list", requirements={"page"="\d+","count"="\d+"})
     */    
    public function listImages(Request $request, $page = 1, $count = 20)
    {       
        $imageRepo = $this->getDoctrine()->getRepository(GalleryImage::class);
        $tag = $request->get('tag');
        
        return $this->render('admin/gallery/list.html.twig', [
            'search' => $tag,
            'imageItems' => $imageRepo->findAllByPage($page, $count, $tag),
            'totalItems' => $imageRepo->getCount($tag),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/upload", name="upload")
     */
    public function upload(ImageService $imageService, Request $request)
    {       
        $form = $this->createForm(ImageUploadType::class, null);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $galleryImage = $imageService->process($form->get('image')->getData(), $form->get('type')->getData());

            $tagRepository = $this->getDoctrine()->getRepository(Tag::class);
            $em = $this->getDoctrine()->getManager();
            
            $tags = array_unique(explode(',', $form->get('tags')->getData()));
            
            foreach($tags as $tag) {
                $t = $tagRepository->findOneBy(['name' => $tag]);
                
                if ($t === null)
                {
                    $t = new Tag();
                    $t->setName($tag);
                    
                    $em->persist($t);
                }
                
                $galleryImage->addTag($t);
            }
            
            $em->persist($galleryImage);
            $em->flush();
            
            $this->addFlash('success', 'Successfully uploaded new image!');
        }

        return $this->render('admin/gallery/upload.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
