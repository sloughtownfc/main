<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;
use App\Entity\Team;
use App\Form\FixtureType;

/**
 * @Route("{teamUrl}/fixture", name="fixture_")
 */
class FixtureController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($teamUrl, $page = 1, $count = 25)
    {
        $fixtureRepo = $this->getDoctrine()->getRepository(Fixture::class);
        
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        return $this->render('admin/fixture/index.html.twig', [
            'fixtureItems' => $fixtureRepo->findAllByTeamAndPage($team, $page, $count),
            'totalItems' => $fixtureRepo->getCount($team),
            'currentPage' => $page,
            'pageSize' => $count,
            'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request, $teamUrl)
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            $this->addFlash('warning', 'Team not found.');
            return $this->redirectToRoute('admin_default_index');
        }
        
        $fixture = new Fixture();
        $fixture->setTeam($team);
        
        $form = $this->createForm(FixtureType::class, $fixture, ['teamId' => $fixture->getTeam()->getId()]);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $fixture = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($fixture);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Fixture!');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/fixture/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fixtureRepo = $em->getRepository(Fixture::class);
        $fixture = $fixtureRepo->findOneById($id);
        
        if (!$fixture) {
            $this->addFlash('warning', 'Fixture not found.');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        if (count($fixture->getStats()) > 0) {
            $this->addFlash('warning', 'Fixture has one or more Stats associated with it.');
            
            return $this->redirectToRoute('admin_opposition_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        $em->remove($fixture);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Fixture!');
        
        return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
        ]);
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $teamUrl, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $fixtureRepo = $em->getRepository(Fixture::class);
        $fixture = $fixtureRepo->findOneById($id);
        
        if (!$fixture) {
            $this->addFlash('warning', 'Fixture not found.');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }

        $form = $this->createForm(FixtureType::class, $fixture, ['teamId' => $fixture->getTeam()->getId()]);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $fixture = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Fixture!');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/fixture/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
    
    /**
     * @Route("/view/{id}", name="view", requirements={"id"="\d+"})
     */
    public function view($teamUrl, $id)
    {
        $fixtureRepo = $this->getDoctrine()->getRepository(Fixture::class);
        $fixture = $fixtureRepo->getFixtureAndCompetitionAndOppositionById($id);
        
        if (!$fixture) {
            $this->addFlash('warning', 'Fixture not found.');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/fixture/view.html.twig', [
            'fixture' => $fixture,
            'teamUrl' => $teamUrl
        ]);
    }
}
