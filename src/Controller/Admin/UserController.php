<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;
use App\Form\AddUserType;
use App\Form\EditUserType;

/**
 * @Route("user", name="user_")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($page = 1, $count = 25)
    {
        $userRepo = $this->getDoctrine()->getRepository(User::class);
        
        return $this->render('admin/user/index.html.twig', [
            'userItems' => $userRepo->findAllByPage($page, $count),
            'totalItems' => $userRepo->getCount(),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        
        $form = $this->createForm(AddUserType::class, $user);       
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            
            $newPassword = $form->get('plainpassword')->getData();
            $user->setPassword($encoder->encodePassword($user, $newPassword));

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Successfully added new User!');
            return $this->redirectToRoute('admin_user_index');
        }
        
        return $this->render('admin/user/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneById($id);
        
        if (!$user) {
            $this->addFlash('warning', 'User not found.');
            return $this->redirectToRoute('admin_user_index');
        }
        
        $em->remove($user);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed User!');
        return $this->redirectToRoute('admin_user_index');
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, UserPasswordEncoderInterface $encoder, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository(User::class);
        $user = $userRepo->findOneById($id);
        
        if (!$user) {
            $this->addFlash('warning', 'User not found.');
            return $this->redirectToRoute('admin_user_index');
        }

        $form = $this->createForm(EditUserType::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();

            $newPassword = $form->get('plainpassword')->getData();
            if ($newPassword) {
                $user->setPassword($encoder->encodePassword($user, $newPassword));
            }
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated User!');
            return $this->redirectToRoute('admin_user_index');
        }
        
        return $this->render('admin/user/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
}
