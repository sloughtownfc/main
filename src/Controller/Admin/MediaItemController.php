<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\MediaItem;
use App\Form\MediaItemType;

/**
 * @Route("media-item", name="media-item_")
 */
class MediaItemController extends AbstractController
{
    /**
     * @Route("/{page}/{count}", name="index", requirements={"page"="\d+","count"="\d+"})
     */
    public function index($page = 1, $count = 25)
    {
        $mediaItemRepo = $this->getDoctrine()->getRepository(MediaItem::class);
        
        return $this->render('admin/media-item/index.html.twig', [
            'mediaItems' => $mediaItemRepo->findAllByPage($page, $count),
            'totalItems' => $mediaItemRepo->getCount(),
            'currentPage' => $page,
            'pageSize' => $count
        ]);
    }
    
    /**
     * @Route("/add", name="add")
     */
    public function add(Request $request)
    {
        $mediaItem = new MediaItem();
        
        $form = $this->createForm(MediaItemType::class, $mediaItem);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $mediaItem = $form->getData();
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($mediaItem);
            $em->flush();
            
            $this->addFlash('success', 'Successfully added new Media Item!');
            return $this->redirectToRoute('admin_media-item_index');
        }
        
        return $this->render('admin/media-item/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Add'
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $mediaItemRepo = $em->getRepository(MediaItem::class);
        $mediaItem = $mediaItemRepo->findOneById($id);
        
        if (!$mediaItem) {
            $this->addFlash('warning', 'Media Item not found.');
            return $this->redirectToRoute('admin_media-item_index');
        }

        $em->remove($mediaItem);
        $em->flush();
            
        $this->addFlash('success', 'Successfully removed Media Item!');
        return $this->redirectToRoute('admin_media-item_index');
    }
    
    /**
     * @Route("/edit/{id}", name="edit", requirements={"id"="\d+"})
     */
    public function edit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $mediaItemRepo = $em->getRepository(MediaItem::class);
        $mediaItem = $mediaItemRepo->findOneById($id);
        
        if (!$mediaItem) {
            $this->addFlash('warning', 'Media Item not found.');
            return $this->redirectToRoute('admin_media-item_index');
        }

        $form = $this->createForm(MediaItemType::class, $mediaItem);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $mediaItem = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Media Item!');
            return $this->redirectToRoute('admin_media-item_index');
        }
        
        return $this->render('admin/media-item/form.html.twig', [
            'form' => $form->createView(),
            'type' => 'Edit'
        ]);
    }
}
