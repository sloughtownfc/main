<?php

namespace App\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;
use App\Entity\Player;
use App\Entity\Stats;
use App\Form\FixtureStatsType;

/**
 * @Route("{teamUrl}/stats", name="stats_")
 */
class StatsController extends AbstractController
{
    /**
     * @Route("/edit/{fixtureId}", name="edit", requirements={"fixtureId"="\d+"})
     */
    public function edit(Request $request, $teamUrl, $fixtureId)
    {
        $em = $this->getDoctrine()->getManager();
        $fixture = $em->getRepository(Fixture::class)->getFixtureAndStatsAndPlayersById($fixtureId);
        
        if (!$fixture) {
            $this->addFlash('warning', 'Fixture not found.');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        $playerCount = $fixture->getCompetition()->getBenchCount() + 11;
        $statsCount = count($fixture->getStats());
        
        if ($statsCount < $playerCount) {
            $required = $playerCount - $statsCount;
        
            for($i = 0; $i < $required; $i++) {               
                $stats = new Stats();
                if ($i + $statsCount < 11) {
                    $stats->setAppearance(true);
                } 
                
                $fixture->addStat($stats);
            }
        }
        
        $players = $em->getRepository(Player::class)->getPartialByTeam($fixture->getTeam()->getId());

        $form = $this->createForm(FixtureStatsType::class, $fixture, ['players' => $players]);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $fixture = $form->getData();
            
            $em->flush();
            
            $this->addFlash('success', 'Successfully updated Fixture!');
            
            return $this->redirectToRoute('admin_fixture_index', [
                'teamUrl' => $teamUrl
            ]);
        }
        
        return $this->render('admin/stats/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
