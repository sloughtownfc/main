<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("arbourpark", name="arbourpark_")
 */
class ArbourParkController extends AbstractController
{    
    /**
     * @Route("", name="index")
     */
    public function index()
    {        
        return $this->render('site/arbourpark/index.html.twig');
    }
    
    /**
     * @Route("/birthdays", name="birthdays")
     */
    public function birthdays()
    {        
        return $this->render('site/arbourpark/birthdays.html.twig');
    }
    
    /**
     * @Route("/meetings", name="meetings")
     */
    public function meetings()
    {        
        return $this->render('site/arbourpark/meetings.html.twig');
    }
    
    /**
     * @Route("/parties", name="parties")
     */
    public function parties()
    {        
        return $this->render('site/arbourpark/parties.html.twig');
    }
    
    /**
     * @Route("/wakes", name="wakes")
     */
    public function wakes()
    {        
        return $this->render('site/arbourpark/wakes.html.twig');
    }
    
    /**
     * @Route("/weddings", name="weddings")
     */
    public function weddings()
    {        
        return $this->render('site/arbourpark/weddings.html.twig');
    }
}
