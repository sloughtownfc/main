<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("", name="squad_")
 */
class SquadController extends AbstractController
{
    /**
     * @Route("squad", name="index")
     */
    public function index()
    {
        return $this->redirectToRoute('site_teams_mens', [], 301);
    }
    
    /**
     * @Route("current-players", name="current_players_redirect")
     */
    public function currentPlayersRedirect()
    {        
        return $this->redirectToRoute('site_teams_mens', [], 301);
    }
    
    /**
     * @Route("player-statistics ", name="player_statistics_redirect")
     */
    public function playerStatisticsRedirect()
    {        
        return $this->redirectToRoute('site_teams_mens', [], 301);
    }
}
