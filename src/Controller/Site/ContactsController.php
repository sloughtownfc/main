<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Message;
use App\Form\MessageType;
use App\Service\EmailerService;
use App\Service\RecaptchaService;

/**
 * @Route("contacts", name="contacts_")
 */
class ContactsController extends AbstractController
{   
    /**
     * @Route("", name="index")
     */
    public function contacts(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {
        $message = new Message();
        
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        
        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendContactEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }
        
        return $this->render('site/contacts/index.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
}