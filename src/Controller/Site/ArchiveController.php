<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Repository\ArchiveRepository;

/**
 * @Route("archive", name="archive_")
 */
class ArchiveController extends AbstractController
{
    private $archiveRepository;
    
    public function __construct(ArchiveRepository $archiveRepository) {
        $this->archiveRepository = $archiveRepository;
    }
    
    /**
     * @Route("", name="index", requirements={"id"="\d+"})
     */
    public function index(Request $request)
    {    
        $results = [];
        if (($query = $request->query->get('query')) && strlen($query) >= 3)
        {
            $results = $this->archiveRepository->search($query);
        }
        
        return $this->render('site/archive/index.html.twig', [
            'results' => $results
        ]);
    }
    
    /**
     * @Route("autocomplete", name="autocomplete")
     */
    public function autocomplete(Request $request)
    {     
        return $this->json($this->archiveRepository->getAutoComplete($request->query->get('term')));
    }
}
