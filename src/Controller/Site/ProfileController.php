<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Player;

/**
 * @Route("profile", name="profile_")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("/{id}/{name}", name="index", requirements={"id"="\d+", "name"=".+"})
     */
    public function index($id)
    {
        $player = $this->getDoctrine()->getRepository(Player::class)->getPlayerById($id);
        if (!$player) {
            return $this->redirectToRoute('site_squad_index');
        }
        
        return $this->render('site/profile/index.html.twig', [
            'player' => $player,
            'statistics' => $this->getDoctrine()->getRepository(Player::class)->getStatistics($player)
        ]);
    }
}
