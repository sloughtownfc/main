<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;

/**
 * @Route("note", name="note_")
 */
class NoteController extends AbstractController
{
    /**
     * @Route("/{id}/{name}", name="index", requirements={"id"="\d+", "name"=".+"})
     */
    public function index($id)
    {            
        return $this->render('site/note/index.html.twig', [
            'note' => $this->getDoctrine()->getRepository(Fixture::class)->getFixtureAndEverythingById($id)
        ]);
    }
}
