<?php

namespace App\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Notification;

class PartialController extends AbstractController
{
    public function notificationArea()
    {       
        return $this->render('site/partial/notification-area.html.twig', [
            'notification' => $this->getDoctrine()->getRepository(Notification::class)->getCurrent()
        ]);
    }
}
