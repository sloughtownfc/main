<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\News;
use App\Entity\Player;

/**
 * @Route("", name="article_")
 */
class ArticleController extends AbstractController
{    
    /**
     * @Route("article/{id}/{name}", name="main", requirements={"id"="\d+", "name"=".+"})
     */
    public function index($id)
    {
        $article = $this->getDoctrine()->getRepository(News::class)->getArticle($id);
        if (!$article) {
            return $this->redirectToRoute('site_news_index');
        }
        
        return $this->render('site/article/index.html.twig', [
            'article' => $article
        ]);
    }
    
    /**
     * @Route("all-time", name="all_time")
     */
    public function allTime()
    {        
        return $this->render('site/article/all-time.html.twig', [
            'stats' => $this->getDoctrine()->getRepository(Player::class)->getAllTimeTop20()
        ]);
    }
    
    /**
     * @Route("cookie-policy", name="cookie_policy")
     */
    public function cookiesPolicy()
    {        
        return $this->render('site/article/cookie-policy.html.twig');
    }
    
    /**
     * @Route("500-club", name="500_club")
     */
    public function fiveHundredClub()
    {        
        return $this->render('site/article/500-club.html.twig');
    }
    
    /**
     * @Route("500_club", name="500_club_redirect")
     */
    public function fiveHundredClubRedirect()
    {        
        return $this->redirectToRoute('site_article_500_club', [], 301);
    }

    /**
     * @Route("32-red", name="32_red")
     */
    public function thirtyTwoRed()
    {        
        return $this->render('site/article/32-red.html.twig');
    }
    
    /**
     * @Route("groundregs", name="groundregs")
     */
    public function groundRegs()
    {        
        return $this->render('site/article/groundregs.html.twig');
    }
    
    /**
     * @Route("honours", name="honours")
     */
    public function honours()
    {        
        return $this->render('site/article/honours.html.twig');
    }

    /**
     * @Route("links", name="links")
     */
    public function links()
    {        
        return $this->render('site/article/links.html.twig');
    }
    
    /**
     * @Route("live", name="live")
     */
    public function live()
    {
        return $this->render('site/article/live.html.twig');
    }

    /**
     * @Route("drinks", name="drinks")
     */
    public function order()
    {        
        return $this->render('site/article/drinks.html.twig');
    }
    
    /**
     * @Route("privacy-policy", name="privacy_policy")
     */
    public function privacyPolicy()
    {        
        return $this->render('site/article/privacy-policy.html.twig');
    }
    
    /**
     * @Route("shop", name="shop_redirect")
     */
    public function shopRedirect()
    {        
        return $this->redirect('https://macronstorecardiff.co.uk/football-clubs/slough-town');
    }
    
    /**
     * @Route("social-guidelines", name="social_guidelines")
     */
    public function socialGuidelines()
    {        
        return $this->render('site/article/social-guidelines.html.twig');
    }
    
    /**
     * @Route("social_guidelines", name="social_guidelines_redirect")
     */
    public function socialGuidelinesRedirect()
    {        
        return $this->redirectToRoute('site_article_social_guidelines', [], 301);
    }    

    /**
     * @Route("partnerships", name="partnerships")
     */
    public function partnerships()
    {        
        return $this->render('site/article/partnerships.html.twig');
    }

    public function sponsorship()
    {        
        return $this->redirectToRoute('site_article_partnerships', [], 301);
    }

    public function sponsorshipOpportunities()
    {        
        return $this->redirectToRoute('site_article_partnerships', [], 301);
    }

    /**
     * @Route("partners", name="partners")
     */
    public function partners()
    {        
        return $this->render('site/article/partners.html.twig');
    }

    
    /**
     * @Route("stfc-youth", name="team_juniors_redirect")
     */
    public function stfcYouthRedirect()
    {        
        return $this->redirectToRoute('site_teams_juniors', [], 301);
    }
    
    /**
     * @Route("terms-of-use", name="terms_of_use")
     */
    public function termsOfUse()
    {        
        return $this->render('site/article/terms-of-use.html.twig');
    }
    
    /**
     * @Route("tickets", name="tickets")
     */
    public function tickets()
    {        
        return $this->render('site/article/tickets.html.twig');
    }
    
    /**
     * @Route("vanarama", name="vanarama")
     */
    public function vanarama()
    {        
        return $this->render('site/article/vanarama.html.twig');
    }
    
    /**
     * @Route("vanarama-insurance", name="vanarama_insurance")
     */
    public function vanaramaInsurance()
    {        
        return $this->redirectToRoute('site_article_vanarama', [], 301);
    }
    
    /**
     * @Route("video/{id}", name="videos_redirect")
     */
    public function videoRedirect($id)
    {        
        return $this->redirectToRoute('site_videos_index', ['id' => $id], 301);
    }

    /**
     * @Route("visiting-arbour-park", name="visiting-arbour-park")
     */
    public function visitingArbourPark()
    {        
        return $this->render('site/article/visiting-arbour-park.html.twig');
    }

    /**
     * @Route("events", name="events_redirect")
     */
    public function eventsRedirect()
    {
        return $this->redirect('https://app.fanbaseclub.com/club/slough-town-fc');
    }

    /**
     * @Route("memberships", name="memberships_redirect")
     */
    public function membershipsRedirect()
    {
        return $this->redirect('https://app.fanbaseclub.com/club/slough-town-fc');
    }

    /**
     * @Route("holidaycamps", name="holidaycamps_redirect")
     */
    public function holidayCampsRedirect()
    {
        return $this->redirect('https://app.fanbaseclub.com/Fan/Fixtures/Index?fanStoreType=Fixtures&clubId=248');
    }

    /**
     * @Route("may", name="may_redirect")
     */
    public function easterRedirect()
    {
        return $this->redirect('https://app.fanbaseclub.com/Fan/Tickets/SelectType?fixtureId=5395');
    }

    /**
     * @Route("may-keeper", name="mayKeeper_redirect")
     */
    public function easterKeeperRedirect()
    {
        return $this->redirect('https://app.fanbaseclub.com/Fan/Tickets/SelectType?fixtureId=5396');
    }

}
