<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\AttendanceEntry;
use App\Entity\LeagueEntry;
use App\Entity\Team;

/**
 * @Route("", name="tables_")
 */
class TableController extends AbstractController
{
    /**
     * @Route("tables/mens/league/attendance", name="league_attendance")
     */
    public function attendance()
    {       
        return $this->render('site/tables/attendance.html.twig', [
            'table' => $this->getDoctrine()->getRepository(AttendanceEntry::class)->findBy([], ['position' => 'ASC'])
        ]);
    }
    
    /**
     * @Route("tables/league/attendance", name="league_attendance_redirect")
     */
    public function attendanceRedirect()
    {       
        return $this->redirectToRoute('site_tables_league_mens_attendance', [], 301);
    }
    
    /**
     * @Route("tables/{teamUrl}/league/{type}", name="league")
     */
    public function league($teamUrl, $type = 'full')
    {   
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        $titleAffix = '';
        
        switch($type) {
            case 'full':
                break;
            case 'home':
                $titleAffix = '- Home Table';
                break;
            case 'away':
                $titleAffix = '- Away Table';
                break;
            case 'goals':
                $titleAffix = '- Goals Scored';
                break;
            case 'conceded':
                $titleAffix = '- Goals Conceded';
                break;
            default:
                return $this->redirectToRoute('site_tables_league', ['teamUrl' => $teamUrl], 301);
        }
        
        return $this->render('site/tables/league.html.twig', [
            'table' => $this->getDoctrine()->getRepository(LeagueEntry::class)->findBy(['team' => $team, 'type' => $type], ['position' => 'ASC']),
            'team' => $team,
            'titleAffix' => $titleAffix,
            'type' => $type
        ]);
    }
    
    /**
     * @Route("tables", name="tables_redirect")
     */
    public function tablesRedirect()
    {        
        return $this->redirectToRoute('site_tables_league', [], 301);
    }
    
    /**
     * @Route("home-league", name="league_home_redirect")
     */
    public function homeLeagueRedirect()
    {        
        return $this->redirectToRoute('site_tables_league', ['type' => 'home'], 301);
    }
    
    /**
     * @Route("away-league", name="league_away_redirect")
     */
    public function awayLeagueRedirect()
    {        
        return $this->redirectToRoute('site_tables_league', ['type' => 'away'], 301);
    }
}
