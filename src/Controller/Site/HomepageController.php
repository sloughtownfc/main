<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;
use App\Entity\LeagueEntry;
use App\Entity\Team;
use App\Repository\HomepageRepository;

/**
 * @Route(name="homepage_")
 */
class HomepageController extends AbstractController
{
    private $homepageRepository;
    
    public function __construct(HomepageRepository $homepageRepository) {
        $this->homepageRepository = $homepageRepository;
    }
    
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {      
        $articles = $this->homepageRepository->getArticles($count = 4);
        for ($i = 0; $i < count($articles); $i++) {
            switch($articles[$i]['type']) {
                case 'news':
                    $articles[$i]['url'] = urldecode($this->generateUrl('site_article_main', ['id' => $articles[$i]['id'], 'name' => str_replace(' ', '-', $articles[$i]['headline'])]));
                    break;
                case 'manager':
                    $articles[$i]['url'] = urldecode($this->generateUrl('site_note_index', ['id' => $articles[$i]['id'], 'name' => str_replace(' ', '-', $articles[$i]['headline'])]));
                    break;
                case 'report':
                    $articles[$i]['url'] = $this->generateUrl('site_reports_index', ['id' => $articles[$i]['id']]);
                    break;
                case 'video':
                    $articles[$i]['url'] = $this->generateUrl('site_videos_index', ['id' => $articles[$i]['id']]);
                    break;
            }
        }
        
        $videos = $this->homepageRepository->getVideos($count = 4);
        for ($i = 0; $i < count($videos); $i++) {
            switch($videos[$i]['type']) {
                case 'news':
                    $videos[$i]['url'] = urldecode($this->generateUrl('site_article_main', ['id' => $videos[$i]['id'], 'name' => str_replace(' ', '-', $videos[$i]['headline'])]));
                    break;
                case 'video':
                    $videos[$i]['url'] = $this->generateUrl('site_videos_index', ['id' => $videos[$i]['id']]);
                    break;
            }
        }
        
        $team = $this->getDoctrine()->getRepository(Team::class)->findOneBy(['url' => $request->get('team') ?? 'mens']);
        
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        return $this->render('site/homepage/index.html.twig', [
            'articles' => $articles,
            'league' => $this->getDoctrine()->getRepository(LeagueEntry::class)->getSnapshot($team, $count = 3),
            'team' => $team,
            'today' => $this->getDoctrine()->getRepository(Fixture::class)->getRandomFixtureForDate(new \DateTime()),
            'videos' => $videos
        ]);
    }
        
    /**
     * @Route("/external-blog/sloughtownsoapbox")
     */
    public function externalRss()
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->get('http://sloughtownsoapbox.blogspot.com/feeds/posts/default?alt=rss');
        
        $response = new Response(
            $res->getBody()->getContents(),
            Response::HTTP_OK,
            ['Content-Type' => 'text/xml']
        );
        
        return $response;
    }
}
