<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;
use App\Entity\Season;
use App\Entity\Team;

/**
 * @Route("fixtures", name="fixtures_")
 */
class FixturesController extends AbstractController
{
    /**
     * @Route("/on-this-day", name="on_this_day")
     */
    public function onThisDay()
    {
        return $this->render('site/fixtures/on-this-day.html.twig', [
            'fixtures' => $this->getDoctrine()->getRepository(Fixture::class)->getFixturesForDate(new \DateTime())
        ]);
    }
    
    /**
     * @Route("/{teamUrl}", name="index")
     */
    public function index(Request $request, $teamUrl)
    {       
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => $teamUrl]);
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        $current = false;
        $seasonQuery = str_replace('-', '/', $request->query->get('season'));
        if (!$seasonQuery || (!$season = $this->getDoctrine()->getRepository(Season::class)->findOneBy(['name' => $seasonQuery]))) {
            $season = $this->getDoctrine()->getRepository(Season::class)->getCurrent();
            $current = true;
        }

        if ($seasonQuery && !$current) {
            $current = $this->getDoctrine()->getRepository(Season::class)->getCurrent()->getName() === $seasonQuery;
        }
        
        $groupedFixtures = [];
        $tbcFixtures = [];
        
        if (!$season->getHideFixtures()) {
            $fixtures = $this->getDoctrine()->getRepository(Fixture::class)->findAllByTeamAndSeason($team, $season, $current);

            foreach ($fixtures as $fixture) {
                if (!empty($fixture->getMatchDate())) {
                    $groupedFixtures[$fixture->getMatchDate()->format('Y-m')][] = $fixture;
                }
                else {
                    $tbcFixtures[] = $fixture;
                }
            }
        }
        
        $seasons = $this->getDoctrine()->getRepository(Season::class)->getSeasonsForTeam($team); 
        
        return $this->render('site/fixtures/index.html.twig', [
            'selectedSeason' => $season,
            'groupedFixtures' => $groupedFixtures,
            'tbcFixtures' => $tbcFixtures,
            'seasons' => $seasons,
            'selected' => $seasonQuery,
            'teamUrl' => $teamUrl,
            'team' => $team
        ]);
    }
    
    /**
     * @Route("", name="index_redirect")
     */
    public function tablesRedirect()
    {        
        return $this->redirectToRoute('site_fixtures_index', ['teamUrl' => 'mens'], 301);
    }
}