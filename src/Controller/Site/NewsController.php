<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\News;
use App\Entity\Season;

/**
 * @Route("news", name="news_")
 */
class NewsController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request)
    {
        $seasonQuery = str_replace('-', '/', $request->query->get('season'));
        if (!$seasonQuery || (!$season = $this->getDoctrine()->getRepository(Season::class)->findOneBy(['name' => $seasonQuery]))) {
            $season = $this->getDoctrine()->getRepository(Season::class)->getCurrent();
        }
        
        $categoryQuery = $request->query->get('category');
        
        $news = $this->getDoctrine()->getRepository(News::class)->findAllBySeason($season, $categoryQuery);
        $seasons = $this->getDoctrine()->getRepository(Season::class)->getNewsList();
        
        return $this->render('site/news/index.html.twig', [
            'news' => $news,
            'seasons' => $seasons,
            'selectedSeason' => $seasonQuery,
            'categories' => News::$categories,
            'selectedCategory' => $categoryQuery
        ]);
    }
}
