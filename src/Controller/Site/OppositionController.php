<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Opposition;

/**
 * @Route("opposition", name="opposition_")
 */
class OppositionController extends AbstractController
{
    /**
     * @Route("/{id}/{name}", name="index", requirements={"id"="\d+", "name"=".+"})
     */
    public function index($id)
    {
        $opposition = $this->getDoctrine()->getRepository(Opposition::class)->getOppositionAndFixturesById($id);
        if (!$opposition) {
            return $this->redirectToRoute('site_database_index');
        }
        
        return $this->render('site/opposition/index.html.twig', [
            'opposition' => $opposition,
            'statistics' => $this->getDoctrine()->getRepository(Opposition::class)->getStatistics($opposition)
        ]);
    }
}
