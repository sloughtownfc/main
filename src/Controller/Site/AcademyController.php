<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Contact;
use App\Entity\Message;
use App\Form\MessageType;
use App\Service\EmailerService;
use App\Service\RecaptchaService;

/**
 * @Route("academy", name="academy_")
 */
class AcademyController extends AbstractController
{    
    /**
     * @Route("", name="index")
     */
    public function index(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {   
        $contactRepo = $this->getDoctrine()->getRepository(Contact::class);
        $contact = $contactRepo->findOneBy(['email' => 'academy@sloughtownfc.net']);
        
        $message = new Message();
        $message->setContact($contact);
        
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendAcademyEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }
        
        return $this->render('site/academy/index.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/elite-development-squad", name="elite_development_squad")
     */
    public function eliteDevelopmentSquad(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {   
        $contactRepo = $this->getDoctrine()->getRepository(Contact::class);
        $contact = $contactRepo->findOneBy(['email' => 'academy@sloughtownfc.net']);
        
        $message = new Message();
        $message->setContact($contact);
        
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendAcademyEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        } 
        return $this->render('site/academy/elite-development-squad.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/full-time-academy", name="full_time_academy")
     */
    public function fullTimeAcademy(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {   
        $contactRepo = $this->getDoctrine()->getRepository(Contact::class);
        $contact = $contactRepo->findOneBy(['email' => 'academy@sloughtownfc.net']);
        
        $message = new Message();
        $message->setContact($contact);
        
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendAcademyEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }      
        return $this->render('site/academy/full-time-academy.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/junior-academy", name="junior_academy")
     */
    public function juniorAcademy(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {   
        $contactRepo = $this->getDoctrine()->getRepository(Contact::class);
        $contact = $contactRepo->findOneBy(['email' => 'academy@sloughtownfc.net']);
        
        $message = new Message();
        $message->setContact($contact);
        
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendAcademyEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }      
        return $this->render('site/academy/junior-academy.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
}
