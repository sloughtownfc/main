<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Opposition;
use App\Entity\Season;

/**
 * @Route("", name="directions_")
 */
class DirectionsController extends AbstractController
{
    /**
     * @Route("how-to-find-us", name="index")
     */
    public function index()
    {       
        return $this->render('site/directions/index.html.twig');
    }
    
    /**
     * @Route("away-directions", name="away_index")
     */
    public function awayDirectionsIndex()
    {       
        $season = $this->getDoctrine()->getRepository(Season::class)->getCurrent();
        
        return $this->render('site/directions/away-directions.html.twig', [
            'oppositions' => $this->getDoctrine()->getRepository(Opposition::class)->findAllAwayBySeason($season)
        ]);
    }
    
    /**
     * @Route("directions/{id}/{name}", name="away_directions", requirements={"id"="\d+", "name"=".+"})
     */
    public function awayDirections($id)
    {
        $opposition = $this->getDoctrine()->getRepository(Opposition::class)->find($id);
        if (!$opposition) {
            return $this->redirectToRoute('site_directions_away_index');
        }
        
        return $this->render('site/directions/away.html.twig', [
            'opposition' => $opposition
        ]);
    }
    
    /**
     * @Route("directions/{id}", name="away_directions_redirect", requirements={"id"="\d+"})
     */
    public function awayDirectionsRedirect($id)
    {
        $opposition = $this->getDoctrine()->getRepository(Opposition::class)->find($id);
        if (!$opposition) {
            return $this->redirectToRoute('site_directions_away_index');
        }
        
        return $this->redirect(urldecode($this->generateUrl('site_directions_away_directions', ['id' => $opposition->getId(), 'name' => $opposition->getUrlName()])));
    }
}
