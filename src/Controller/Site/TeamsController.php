<?php

namespace App\Controller\Site;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Contact;
use App\Entity\Message;
use App\Entity\Player;
use App\Entity\Team;
use App\Form\FixedMessageType;
use App\Service\EmailerService;
use App\Service\RecaptchaService;

/**
 * @Route("teams", name="teams_")
 */
class TeamsController extends AbstractController
{
    /**
     * @Route("/mens", name="mens")
     */
    public function mens()
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => 'mens']);
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        $players = $this->getDoctrine()->getRepository(Player::class)->getCurrentSquadByTeam($team);
        $groupedPlayers = [];
        
        foreach($players as $player) {
            $groupedPlayers[$player['position']][] = $player;
        }

        return $this->render('site/teams/mens.html.twig', [
            'groupedPlayers' => $groupedPlayers
        ]);
    }
    
    /**
     * @Route("/community", name="community")
     */
    public function community(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {
        $message = new Message();
        
        $form = $this->createForm(FixedMessageType::class, $message);
        $form->handleRequest($request);
        
        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $contact = $this->getDoctrine()->getRepository(Contact::class)->find(11);
                    $message->setContact($contact);
                    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendCommunityEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }
        
        return $this->render('site/teams/community.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/juniors", name="juniors")
     */
    public function juniors(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {
        $message = new Message();
        
        $form = $this->createForm(FixedMessageType::class, $message);
        $form->handleRequest($request);
        
        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $contact = $this->getDoctrine()->getRepository(Contact::class)->find(1);
                    $message->setContact($contact);
                    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendJuniorsEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }
        
        return $this->render('site/teams/juniors.html.twig', [
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/ladies", name="ladies")
     */
    public function ladies(EmailerService $emailerService, RecaptchaService $recaptchaService, Request $request)
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => 'ladies']);
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        $players = $this->getDoctrine()->getRepository(Player::class)->getCurrentSquadByTeam($team);
        $groupedPlayers = [];
        
        foreach($players as $player) {
            $groupedPlayers[$player['position']][] = $player;
        }
        
        $message = new Message();
        
        $form = $this->createForm(FixedMessageType::class, $message);
        $form->handleRequest($request);
        
        $showForm = true;
        $recaptchaError = false;
        
        if ($form->isSubmitted()) {
            if ($recaptchaService->verify($request->get('g-recaptcha-response'))) {
                if ($form->isValid()) {
                    $message = $form->getData();

                    $contact = $this->getDoctrine()->getRepository(Contact::class)->find(1);
                    $message->setContact($contact);
                    
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($message);
                    $em->flush();

                    $emailerService->sendLadiesEmail($message->getContact()->getEmail(), $message->getName(), $message->getReturnEmail(), $message->getSubject(), $message->getBody());
                    
                    $showForm = false;
                }
            }
            else {
                $recaptchaError = true;
            }
        }
        
        return $this->render('site/teams/ladies.html.twig', [
            'groupedPlayers' => $groupedPlayers,
            'form' => $form->createView(),
            'showForm' => $showForm,
            'recaptchaError' => $recaptchaError
        ]);
    }
    
    /**
     * @Route("/elite-development-squad", name="elite_development_squad")
     */
    public function eliteDevelopmentSquad()
    {
        $teamRepo = $this->getDoctrine()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => 'elite-development-squad']);
        if (!$team) {
            return $this->redirectToRoute('site_homepage_index');
        }
        
        $players = $this->getDoctrine()->getRepository(Player::class)->getCurrentSquadByTeam($team);
        $groupedPlayers = [];
        
        foreach($players as $player) {
            $groupedPlayers[$player['position']][] = $player;
        }

        return $this->render('site/teams/elite-development-squad.html.twig', [
            'groupedPlayers' => $groupedPlayers
        ]);
    }
    
    /**
     * @Route("/u23s", name="u23s_redirect")
     */
    public function u23sRedirect()
    {        
        return $this->redirectToRoute('site_teams_elite_development_squad', [], 301);
    }
}
