<?php

namespace App\Controller\Site;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Fixture;

/**
 * @Route("videos", name="videos_")
 */
class VideosController extends AbstractController
{
    /**
     * @Route("/{id}", name="index", requirements={"id"="\d+"})
     */
    public function index($id)
    {     
        $report = $this->getDoctrine()->getRepository(Fixture::class)->getFixtureAndEverythingById($id);
        if (!$report) {
            return $this->redirectToRoute('site_fixtures_index', ['teamUrl' => 'mens']);
        }
        
        return $this->render('site/videos/index.html.twig', [
            'report' => $report,
        ]);
    }
}
