<?php

namespace App\Mapping;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\MappingOperation\Operation;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use App\Dto\CompetitionDto;
use App\Dto\LeagueEntryDto;
use App\Dto\OppositionDto;
use App\Dto\TeamDto;
use App\Dto\FixtureDto;
use App\Entity\Competition;
use App\Entity\Fixture;
use App\Entity\LeagueEntry;
use App\Entity\Opposition;
use App\Entity\Team;

class AutoMapperConfig implements AutoMapperConfiguratorInterface
{
    private $router;
    
    public function __construct(UrlGeneratorInterface $router) {
        $this->router = $router;
    }
    
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(Competition::class, CompetitionDto::class);
        
        $config->registerMapping(LeagueEntry::class, LeagueEntryDto::class)
            ->forMember('goalDifference', function($source) {
               return $source->getGoalDifference();
            });
               
        $config->registerMapping(Opposition::class, OppositionDto::class);
        $config->registerMapping(Team::class, TeamDto::class);
        
        $config->registerMapping(Fixture::class, FixtureDto::class)
            ->forMember('matchDate', function($source) {
                return $source->getMatchDate()->format('Y-m-d');
            })
            ->forMember('kickoff', function($source) {
                if (!$source->getKickoff()) {
                    return null;
                }

                return $source->getKickoff()->format('H:i');
            })
            ->forMember('videoUrl', function($source) {
                return $this->router->generate('site_videos_index', ['id' => $source->getId()]);
            })
            ->forMember('videoPublished', function($source) {
                if (!$source->getVideoPublished()) {
                    return false;
                }

                return $source->getVideoPublished();
            })
            ->forMember('liveStreamUrl', Operation::fromProperty('liveStreamId'))
            ->forMember('reportUrl', function($source) {
                return $this->router->generate('site_reports_index', ['id' => $source->getId()]);
            })
            ->forMember('reportPublished', function($source) {
                if (!$source->getReportPublished()) {
                    return false;
                }

                return $source->getReportPublished();
            })
            ->forMember('competition', Operation::mapTo(CompetitionDto::class))
            ->forMember('opposition', Operation::mapTo(OppositionDto::class))
            ->forMember('team', Operation::mapTo(TeamDto::class));
    }
}