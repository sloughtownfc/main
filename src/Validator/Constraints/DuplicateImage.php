<?php

namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DuplicateImage extends Constraint
{
    public $message = 'The uploaded image already exists within the gallery.';
}