<?php

namespace App\Validator\Constraints;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use App\Repository\GalleryImageRepository;

class DuplicateImageValidator extends ConstraintValidator
{
    private $repository;
    
    public function __construct(GalleryImageRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof DuplicateImage) {
            throw new UnexpectedTypeException($constraint, DuplicateImage::class);
        }
        
        if (null === $value || '' === $value) {
            return;
        }

        if (!$value instanceof File) {
            throw new UnexpectedTypeException($value, File::class);
        }
        
        if ($this->repository->isDuplicate(md5_file($value->getRealPath())) === true) {
            $this->context->buildViolation($constraint->message)
                ->addViolation();

            return;
        }
    }
}
