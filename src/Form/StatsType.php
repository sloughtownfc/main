<?php

namespace App\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Player;
use App\Entity\Stats;

class StatsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {        
        $builder
            ->add('shirt', IntegerType::class, ['required' => false])
            ->add('sub_shirt', IntegerType::class, ['required' => false])
            ->add('appearance')
            ->add('goals', IntegerType::class, ['required' => false])
            ->add('bookings', ChoiceType::class, [
                'choices' => [
                    'No' => 0,
                    'Yellow' => 1,
                    'Red' => 2
                ]
            ])
            ->add('injured')
            ->add('player', EntityType::class, [
                'class' => Player::class,
                'choices' => $options['players'],
                'choice_label' => 'listName'
            ])
        ;

    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Stats::class,
            'players' => []
        ]);
        
        $resolver->setAllowedTypes('players', 'App\Entity\Player[]');
    }
}
