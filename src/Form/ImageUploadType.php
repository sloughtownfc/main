<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Validator\Constraints\DuplicateImage;

class ImageUploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Article' => 'article',
                    'Opposition Badge' => 'badge',
                    'Player' => 'player'
                ],
                'constraints' => [
                    new NotBlank()                    
                ],
                'expanded' => true,
                'multiple' => false,
                'required' => true
            ])
            ->add('image', FileType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Image(),
                    new DuplicateImage()
                ],
                'required' => true
            ])
            ->add('tags', TextType::class, [
                'constraints' => [
                    new NotBlank()                    
                ],
                'required' => true
            ])
            ->add('upload', SubmitType::class)
        ;
    }
}
