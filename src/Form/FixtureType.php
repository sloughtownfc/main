<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Competition;
use App\Entity\Fixture;
use App\Entity\Opposition;
use App\Entity\Player;
use App\Entity\Team;
use App\Repository\CompetitionRepository;
use App\Repository\OppositionRepository;
use App\Repository\PlayerRepository;

class FixtureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('team', EntityType::class, [
                'class' => Team::class,
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('t')->where('t.id = :team')->setParameter('team', $options['teamId']);
                },
                'choice_label' => 'shortName',
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('matchDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'required' => false
            ])
            ->add('isHome')
            ->add('ticketsUrl')
            ->add('liveStreamId')
            ->add('kickoff', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'HH:mm',
                'required' => false
            ])
            ->add('matchSponsor')
            ->add('ballSponsor')
            ->add('reportAuthor')
            ->add('reportPictureUrl')
            ->add('reportHomeScore')
            ->add('reportAwayScore')
            ->add('reportHomeScorers')
            ->add('reportAwayScorers')
            ->add('reportExtraTime')
            ->add('reportAttendance')
            ->add('reportHeadline')
            ->add('reportExcerpt')
            ->add('reportBody')
            ->add('reportFriendlyTeam')
            ->add('reportMom', EntityType::class, [
                'class' => Player::class,
                'query_builder' => function (PlayerRepository $er) use ($options) {
                    return $er->getPartialByTeamQb($options['teamId']);
                },
                'choice_label' => 'listName',
                'placeholder' => 'Please Select',
                'required' => false
            ])
            ->add('reportAwayTeam')
            ->add('reportFlickrUrl')
            ->add('reportPublished', CheckboxType::class, ['required' => false])
            ->add('managerNoteAuthor')
            ->add('managerNoteHeadline')
            ->add('managerNoteExcerpt')
            ->add('managerNoteBody')
            ->add('managerNotePictureUrl')
            ->add('managerNotePictureThumbnailUrl')
            ->add('managerNoteCaption')
            ->add('managerNoteQuote')
            ->add('managerNoteQuoteCredit')
            ->add('managerNotePublished', CheckboxType::class, ['required' => false])
            ->add('coachDeparts')
            ->add('coachAdultPrice')
            ->add('coachReducedPrice')
            ->add('videoAuthor')
            ->add('videoPictureUrl')
            ->add('videoPictureThumbnailUrl')
            ->add('videoUrl')
            ->add('videoBody')
            ->add('videoHeadline')
            ->add('videoExcerpt')
            ->add('videoPublished', CheckboxType::class, ['required' => false])
            ->add('notification')
            ->add('competition', EntityType::class, [
                'class' => Competition::class,
                'query_builder' => function (CompetitionRepository $er) use ($options) {
                    return $er->getPartialByTeamQb($options['teamId']);
                },
                'choice_label' => 'name',
                'placeholder' => 'Please Select'
            ])
            ->add('opposition', EntityType::class, [
                'class' => Opposition::class,
                'query_builder' => function (OppositionRepository $er) use ($options) {
                    return $er->getPartialByTeamQb($options['teamId']);
                },
                'choice_label' => 'name',
                'placeholder' => 'Please Select'
            ])
            ->add('save', SubmitType::class)
        ;

    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fixture::class,
            'teamId' => null
        ]);
        
        $resolver->setAllowedTypes('teamId', 'int');
    }
}
