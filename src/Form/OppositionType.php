<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Opposition;
use App\Entity\Team;

class OppositionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('team', EntityType::class, [
                'class' => Team::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.id', 'ASC');
                },
                'choice_label' => 'shortName',
            ])
            ->add('name')
            ->add('logo')
            ->add('website')
            ->add('groundsVenue')
            ->add('groundsAddress')
            ->add('groundsTelephone')
            ->add('groundsDirections')
            ->add('groundsLat')
            ->add('groundsLng')
            ->add('notes')
            ->add('save', SubmitType::class)
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Opposition::class,
        ]);
    }
}
