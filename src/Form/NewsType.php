<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use App\Entity\News;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', ChoiceType::class, [
                'choices' => News::$categories,
                'constraints' => [
                    new NotBlank()                    
                ],
                'multiple' => false,
                'required' => true
            ])
            ->add('author')
            ->add('headline')
            ->add('publishDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy'
            ])
            ->add('publishTime', DateTimeType::class, [
                'input' => 'datetime',
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'HH:mm'
            ])
            ->add('excerpt')
            ->add('body')
            ->add('pictureUrl')
            ->add('smallPictureUrl')
            ->add('caption')
            ->add('quote')
            ->add('quoteBy')
            ->add('videoUrl')
            ->add('audioUrl')
            ->add('save', SubmitType::class)
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
