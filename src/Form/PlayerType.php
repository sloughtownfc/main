<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Player;
use App\Entity\Position;
use App\Entity\Team;

class PlayerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('teams', EntityType::class, [
                'class' => Team::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('t')->orderBy('t.id', 'ASC');
                },
                'choice_label' => 'shortName',
                'multiple' => true,
                'expanded' => true
            ])
            ->add('firstName')
            ->add('lastName')
            ->add('dob', DateType::class, [
                'years' => range( date('Y'), date('1900')),
                'widget' => 'single_text',
                'html5' => false,
                'format' => 'dd/MM/yyyy',
                'required' => false
            ])                
            ->add('description')
            ->add('achievements')
            ->add('pictureUrl')
            ->add('squadNumber')
            ->add('sponsoredBy')
            ->add('onLoan')
            ->add('active')
            ->add('position', EntityType::class, [
                'class' => Position::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')->orderBy('p.name', 'ASC');
                },
                'choice_label' => 'name',
            ])
            ->add('title', null, [
                'help' => 'Set the title  for management. Leave as blank for players.'
            ])
            ->add('orderBy', null, [
                'help' => 'Set the order rank for management, ordered descending. Leave as 0 for players.'
            ])
            ->add('save', SubmitType::class)
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Player::class,
        ]);
    }
}
