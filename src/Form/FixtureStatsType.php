<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Entity\Fixture;
use App\Entity\Stats;
use App\Form\StatsType;

class FixtureStatsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('stats', CollectionType::class, [
                'entry_type' => StatsType::class,
                'entry_options' => ['label' => false, 'empty_data' => null, 'players' => $options['players']],
                'by_reference' => false,
                'allow_delete' => true,
                'delete_empty' => function (Stats $stats = null) {
                    return null === $stats || empty($stats->getShirt()) || $stats->getShirt() === 0;
                },
            ])
            ->add('save', SubmitType::class)
        ;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Fixture::class,
            'players' => []
        ]);
        
        $resolver->setAllowedTypes('players', 'App\Entity\Player[]');
    }
}
