<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * U18sFixture
 *
 * @ORM\Table(name="u18s_fixtures")
 * @ORM\Entity
 */
class U18sFixture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="match_date", type="date", nullable=true)
     */
    private $matchDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="competition", type="string", length=64, nullable=true)
     */
    private $competition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="opposition", type="string", length=64, nullable=true)
     */
    private $opposition;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_home", type="boolean", nullable=true)
     */
    private $isHome;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="kickoff", type="time", nullable=true)
     */
    private $kickoff;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="report_home_score", type="boolean", nullable=true)
     */
    private $reportHomeScore;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="report_away_score", type="boolean", nullable=true)
     */
    private $reportAwayScore;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_home_scorers", type="text", length=65535, nullable=true)
     */
    private $reportHomeScorers;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_headline", type="string", length=64, nullable=true)
     */
    private $reportHeadline;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_excerpt", type="text", length=65535, nullable=true)
     */
    private $reportExcerpt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_body", type="text", length=65535, nullable=true)
     */
    private $reportBody;

    /**
     * @var string|null
     *
     * @ORM\Column(name="picture_url", type="string", length=128, nullable=true)
     */
    private $pictureUrl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_team", type="text", length=65535, nullable=true)
     */
    private $reportTeam;

    /**
     * @var string|null
     *
     * @ORM\Column(name="report_motm", type="string", length=64, nullable=true)
     */
    private $reportMotm;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="report_published", type="boolean", nullable=true)
     */
    private $reportPublished;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchDate(): ?\DateTimeInterface
    {
        return $this->matchDate;
    }

    public function setMatchDate(?\DateTimeInterface $matchDate): self
    {
        $this->matchDate = $matchDate;

        return $this;
    }

    public function getCompetition(): ?string
    {
        return $this->competition;
    }

    public function setCompetition(?string $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    public function getOpposition(): ?string
    {
        return $this->opposition;
    }

    public function setOpposition(?string $opposition): self
    {
        $this->opposition = $opposition;

        return $this;
    }

    public function getIsHome(): ?bool
    {
        return $this->isHome;
    }

    public function setIsHome(?bool $isHome): self
    {
        $this->isHome = $isHome;

        return $this;
    }

    public function getKickoff(): ?\DateTimeInterface
    {
        return $this->kickoff;
    }

    public function setKickoff(?\DateTimeInterface $kickoff): self
    {
        $this->kickoff = $kickoff;

        return $this;
    }

    public function getReportHomeScore(): ?bool
    {
        return $this->reportHomeScore;
    }

    public function setReportHomeScore(?bool $reportHomeScore): self
    {
        $this->reportHomeScore = $reportHomeScore;

        return $this;
    }

    public function getReportAwayScore(): ?bool
    {
        return $this->reportAwayScore;
    }

    public function setReportAwayScore(?bool $reportAwayScore): self
    {
        $this->reportAwayScore = $reportAwayScore;

        return $this;
    }

    public function getReportHomeScorers(): ?string
    {
        return $this->reportHomeScorers;
    }

    public function setReportHomeScorers(?string $reportHomeScorers): self
    {
        $this->reportHomeScorers = $reportHomeScorers;

        return $this;
    }

    public function getReportHeadline(): ?string
    {
        return $this->reportHeadline;
    }

    public function setReportHeadline(?string $reportHeadline): self
    {
        $this->reportHeadline = $reportHeadline;

        return $this;
    }

    public function getReportExcerpt(): ?string
    {
        return $this->reportExcerpt;
    }

    public function setReportExcerpt(?string $reportExcerpt): self
    {
        $this->reportExcerpt = $reportExcerpt;

        return $this;
    }

    public function getReportBody(): ?string
    {
        return $this->reportBody;
    }

    public function setReportBody(?string $reportBody): self
    {
        $this->reportBody = $reportBody;

        return $this;
    }

    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    public function setPictureUrl(?string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    public function getReportTeam(): ?string
    {
        return $this->reportTeam;
    }

    public function setReportTeam(?string $reportTeam): self
    {
        $this->reportTeam = $reportTeam;

        return $this;
    }

    public function getReportMotm(): ?string
    {
        return $this->reportMotm;
    }

    public function setReportMotm(?string $reportMotm): self
    {
        $this->reportMotm = $reportMotm;

        return $this;
    }

    public function getReportPublished(): ?bool
    {
        return $this->reportPublished;
    }

    public function setReportPublished(?bool $reportPublished): self
    {
        $this->reportPublished = $reportPublished;

        return $this;
    }


}
