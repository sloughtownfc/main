<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Opposition
 *
 * @ORM\Table(name="oppositions", indexes={@ORM\Index(name="idx_oppositions_autocomplete", columns={"name"})})
 * @ORM\Entity(repositoryClass="App\Repository\OppositionRepository")
 */
class Opposition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 64)
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="logo", type="string", length=128, nullable=false)
     */
    private $logo;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="website", type="string", length=128, nullable=true)
     */
    private $website;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="grounds_venue", type="string", length=128, nullable=true)
     */
    private $groundsVenue;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="grounds_address", type="string", length=128, nullable=true)
     */
    private $groundsAddress;

    /**
     * @var string|null
     * @Assert\Length(max = 11)
     * @ORM\Column(name="grounds_telephone", type="string", length=11, nullable=true)
     */
    private $groundsTelephone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="grounds_directions", type="text", length=65535, nullable=true)
     */
    private $groundsDirections;

    /**
     * @var string|null
     * @Assert\Length(max = 50)
     * @ORM\Column(name="grounds_lat", type="string", length=50, nullable=true)
     */
    private $groundsLat;
    
    /**
     * @var string|null
     * @Assert\Length(max = 50)
     * @ORM\Column(name="grounds_lng", type="string", length=50, nullable=true)
     */
    private $groundsLng;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", length=65535, nullable=true)
     */
    private $notes;
    
    /**
     * @ORM\OneToMany(targetEntity="Fixture", mappedBy="opposition")
     */
    private $fixtures;

    /**
     * @var \Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="oppositions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     * })
     */
    private $team;
    
    public function __construct()
    {
        $this->fixtures = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getUrlName(): string
    {
        return empty($this->name) ? '' : str_replace(' ', '-', $this->name);
    }
    
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getGroundsVenue(): ?string
    {
        return $this->groundsVenue;
    }

    public function setGroundsVenue(?string $groundsVenue): self
    {
        $this->groundsVenue = $groundsVenue;

        return $this;
    }

    public function getGroundsAddress(): ?string
    {
        return $this->groundsAddress;
    }

    public function setGroundsAddress(?string $groundsAddress): self
    {
        $this->groundsAddress = $groundsAddress;

        return $this;
    }

    public function getGroundsTelephone(): ?string
    {
        return $this->groundsTelephone;
    }

    public function setGroundsTelephone(?string $groundsTelephone): self
    {
        $this->groundsTelephone = $groundsTelephone;

        return $this;
    }

    public function getGroundsDirections(): ?string
    {
        return $this->groundsDirections;
    }

    public function setGroundsDirections(?string $groundsDirections): self
    {
        $this->groundsDirections = $groundsDirections;

        return $this;
    }

    public function getGroundsLat(): ?string
    {
        return $this->groundsLat;
    }

    public function setGroundsLat(?string $groundsLat): self
    {
        $this->groundsLat = $groundsLat;

        return $this;
    }

    public function getGroundsLng(): ?string
    {
        return $this->groundsLng;
    }

    public function setGroundsLng(?string $groundsLng): self
    {
        $this->groundsLng = $groundsLng;

        return $this;
    }
    
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * @return Collection|Fixture[]
     */
    public function getFixtures(): Collection
    {
        return $this->fixtures;
    }

    public function addFixture(Fixture $fixture): self
    {
        if (!$this->fixtures->contains($fixture)) {
            $this->fixtures[] = $fixture;
        }

        return $this;
    }

    public function removeFixture(Fixture $fixture): self
    {
        if ($this->fixtures->contains($fixture)) {
            $this->fixtures->removeElement($fixture);
        }

        return $this;
    }
    
    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }
}
