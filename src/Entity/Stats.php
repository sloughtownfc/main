<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Stats
 *
 * @ORM\Table(name="stats", indexes={@ORM\Index(name="fk_stats_player_id_idx", columns={"player_id"}), @ORM\Index(name="fk_stats_fixture_id_idx", columns={"fixture_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Stats
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\Column(name="shirt", type="integer", nullable=false, options={"default": 0})
     */
    private $shirt;

    /**
     * @var int
     * 
     * @ORM\Column(name="sub_shirt", type="integer", nullable=false, options={"default": 0})
     */
    private $subShirt;

    /**
     * @var bool
     *
     * @ORM\Column(name="appearance", type="boolean", nullable=false)
     */
    private $appearance;

    /**
     * @var int
     * 
     * @ORM\Column(name="goals", type="integer", nullable=false, options={"default": 0})
     */
    private $goals;

    /**
     * @var int
     * 
     * @ORM\Column(name="bookings", type="integer", nullable=false, options={"default": 0})
     */
    private $bookings;

    /**
     * @var bool
     *
     * @ORM\Column(name="injured", type="boolean", nullable=false)
     */
    private $injured;

    /**
     * @var \Fixture
     *
     * @ORM\ManyToOne(targetEntity="Fixture", inversedBy="stats")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fixture_id", referencedColumnName="id")
     * })
     */
    private $fixture;

    /**
     * @var \Player
     *
     * @ORM\ManyToOne(targetEntity="Player", inversedBy="stats")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     * })
     */
    private $player;

    public function __construct()
    {
        $this->bookings = 0;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getShirt(): ?int
    {
        return $this->shirt;
    }

    public function setShirt(?int $shirt): self
    {
        $this->shirt = $shirt;

        return $this;
    }

    public function getSubShirt(): ?int
    {
        return $this->subShirt;
    }

    public function setSubShirt(?int $subShirt): self
    {
        $this->subShirt = $subShirt;

        return $this;
    }

    public function getAppearance(): ?bool
    {
        return $this->appearance;
    }

    public function setAppearance(bool $appearance): self
    {
        $this->appearance = $appearance;

        return $this;
    }

    public function getGoals(): ?int
    {
        return $this->goals;
    }

    public function setGoals(?int $goals): self
    {
        $this->goals = $goals;

        return $this;
    }

    public function getBookings(): ?int
    {
        return $this->bookings;
    }

    public function setBookings(int $bookings): self
    {
        $this->bookings = $bookings;

        return $this;
    }

    public function getInjured(): ?bool
    {
        return $this->injured;
    }

    public function setInjured(bool $injured): self
    {
        $this->injured = $injured;

        return $this;
    }

    public function getFixture(): ?Fixture
    {
        return $this->fixture;
    }

    public function setFixture(?Fixture $fixture): self
    {
        $this->fixture = $fixture;

        return $this;
    }

    public function getPlayer(): ?Player
    {
        return $this->player;
    }

    public function setPlayer(?Player $player): self
    {
        $this->player = $player;

        return $this;
    }

    /**
    * @ORM\PrePersist
    */
   public function setRequiredFields()
   {
        if (!isset($this->subShirt)) {
            $this->subShirt = 0;
        }
       
        if (!isset($this->goals)) {
           $this->goals = 0;
        }
   }
}
