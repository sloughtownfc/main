<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Player
 *
 * @ORM\Table(name="players", indexes={@ORM\Index(name="fk_players_position_id_idx", columns={"position_id"}), @ORM\Index(name="idx_players_autocomplete", columns={"full_name"})})
 * @ORM\Entity(repositoryClass="App\Repository\PlayerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=32, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=32, nullable=false)
     */
    private $lastName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="achievements", type="text", length=65535, nullable=true)
     */
    private $achievements;

    /**
     * @var string
     *
     * @ORM\Column(name="picture_url", type="string", length=128, nullable=false)
     */
    private $pictureUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="sponsored_by", type="string", length=128, nullable=true)
     */
    private $sponsoredBy;
    
    /**
     * @var int|null
     *
     * @ORM\Column(name="squad_number", type="integer", nullable=true)
     */
    private $squadNumber;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;
    
    /**
     * @var string
     *
     * @ORM\Column(name="full_name", type="string", length=100, nullable=false)
     */
    private $fullName;
    
    /**
     * @var \Position
     *
     * @ORM\ManyToOne(targetEntity="Position")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     * })
     */
    private $position;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="on_loan", type="boolean", nullable=false, options={"default": 0})
     */
    private $onLoan;
    
    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=100, nullable=true)
     */
    private $title;
    
    /**
     * @var int
     *
     * @ORM\Column(name="order_by", type="integer", nullable=false, options={"default": 0})
     */
    private $orderBy;
    
    /**
     * @ORM\OneToMany(targetEntity="Stats", mappedBy="player", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $stats;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Team", inversedBy="players")
     * @ORM\JoinTable(name="players_teams",
     *   joinColumns={
     *     @ORM\JoinColumn(name="player_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     *   }
     * )
     */
    private $teams;
    
    public function __construct()
    {
        $this->stats = new \Doctrine\Common\Collections\ArrayCollection();
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->orderBy = 0;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getListName(): ?string
    {
        if (empty($this->getLastName())) {
            return $this->getFirstName();
        }

        return $this->getLastName() . ', ' . $this->getFirstName();
    }
    
    public function getName(): ?string
    {
        if (empty($this->getLastName())) {
            return $this->getFirstName();
        }

        return $this->getFirstName() . ' ' . $this->getLastName();
    }
    
    public function getUrlName(): string
    {
        return str_replace(' ', '-', $this->getName());
    }
    
    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(?\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getAchievements(): ?string
    {
        return $this->achievements;
    }

    public function setAchievements(?string $achievements): self
    {
        $this->achievements = $achievements;

        return $this;
    }

    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    public function setPictureUrl(string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    public function getSquadNumber(): ?int
    {
        return $this->squadNumber;
    }

    public function setSquadNumber(?int $squadNumber): self
    {
        $this->squadNumber = $squadNumber;

        return $this;
    }
    
    public function getSponsoredBy(): ?string
    {
        return $this->sponsoredBy;
    }

    public function setSponsoredBy(?string $sponsoredBy): self
    {
        $this->sponsoredBy = $sponsoredBy;

        return $this;
    }
    
    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPosition(): ?Position
    {
        return $this->position;
    }

    public function setPosition(?Position $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getOnLoan(): ?bool
    {
        return $this->onLoan;
    }

    public function setOnLoan(bool $onLoan): self
    {
        $this->onLoan = $onLoan;

        return $this;
    }
    
    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
    
    public function getOrderBy(): int
    {
        return $this->orderBy;
    }

    public function setOrderBy(int $orderBy): self
    {
        $this->orderBy = $orderBy;

        return $this;
    }
    
    /**
     * @return Collection|Stats[]
     */
    public function getStats(): Collection
    {
        return $this->stats;
    }

    public function addStat(Stats $stats): self
    {
        if (!$this->stats->contains($stats)) {
            $this->stats[] = $stats;
            $stats->setPlayer($this);
        }

        return $this;
    }

    public function removeStat(Stats $stats): self
    {
        if ($this->stats->contains($stats)) {
            $this->stats->removeElement($stats);
            $stats->setPlayer(null);
        }

        return $this;
    }
    
    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function setFullname()
    {
        $fullname = $this->firstName . ' ' . $this->lastName;
        
        if ($this->firstName === 'Unknown') {
            $fullname = $this->lastName;
        }
        
        $this->fullName = $fullname;
    }
    
    /**
     * @return Collection|Team[]
     */
    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function addTeam(Team $team): self
    {
        if (!$this->teams->contains($team)) {
            $this->teams[] = $team;
            $team->addPlayer($this);
        }

        return $this;
    }

    public function removeTeam(Team $team): self
    {
        if ($this->teams->contains($team)) {
            $this->teams->removeElement($team);
            $team->removePlayer($this);
        }

        return $this;
    }
}
