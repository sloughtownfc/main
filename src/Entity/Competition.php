<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Competition
 *
 * @ORM\Table(name="competitions")
 * @ORM\Entity(repositoryClass="App\Repository\CompetitionRepository")
 */
class Competition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 64)
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="description", type="string", length=128, nullable=false)
     */
    private $description;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="bench_count", type="integer", nullable=false, options={"default"="5"})
     */
    private $benchCount = '5';

    /**
     * @var bool
     *
     * @ORM\Column(name="stats_recorded", type="boolean", nullable=false, options={"default"="1"})
     */
    private $statsRecorded = '1';

    /**
     * @ORM\OneToMany(targetEntity="Fixture", mappedBy="competition")
     */
    private $fixtures;
    
    /**
     * @var \Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="competitions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     * })
     */
    private $team;
    
    public function __construct()
    {
        $this->fixtures = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBenchCount(): ?int
    {
        return $this->benchCount;
    }

    public function setBenchCount(int $benchCount): self
    {
        $this->benchCount = $benchCount;

        return $this;
    }

    public function getStatsRecorded(): ?bool
    {
        return $this->statsRecorded;
    }

    public function setStatsRecorded(bool $statsRecorded): self
    {
        $this->statsRecorded = $statsRecorded;

        return $this;
    }

    /**
     * @return Collection|Fixture[]
     */
    public function getFixtures(): Collection
    {
        return $this->fixtures;
    }

    public function addFixture(Fixture $fixture): self
    {
        if (!$this->fixtures->contains($fixture)) {
            $this->fixtures[] = $fixture;
        }

        return $this;
    }

    public function removeFixture(Fixture $fixture): self
    {
        if ($this->fixtures->contains($fixture)) {
            $this->fixtures->removeElement($fixture);
        }

        return $this;
    }
    
    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }
}
