<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LeagueEntry
 *
 * @ORM\Table(name="league_entries")
 * @ORM\Entity(repositoryClass="App\Repository\LeagueEntryRepository")
 */
class LeagueEntry
{
    /**
     * @var \Team
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="leagueEntries")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     * })
     */
    private $team;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 10)
     * @ORM\Id
     * @ORM\Column(name="`type`", type="string", length=10, nullable=false)
     */
    private $type;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Id
     * @ORM\Column(name="`position`", type="integer", nullable=false)
     */
    private $position;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="`name`", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="played", type="integer", nullable=false)
     */
    private $played;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="won", type="integer", nullable=false)
     */
    private $won;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="drawn", type="integer", nullable=false)
     */
    private $drawn;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="lost", type="integer", nullable=false)
     */
    private $lost;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="`for`", type="integer", nullable=false)
     */
    private $for;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="`against`", type="integer", nullable=false)
     */
    private $against;
    
    /**
     * @var int
     * @Assert\NotBlank()
     * @ORM\Column(name="points", type="integer", nullable=false)
     */
    private $points;
    
    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }
    
    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
    
    public function getPlayed(): int
    {
        return $this->played;
    }

    public function setPlayed(int $played): self
    {
        $this->played = $played;

        return $this;
    }
    
    public function getWon(): int
    {
        return $this->won;
    }

    public function setWon(int $won): self
    {
        $this->won = $won;

        return $this;
    }
    
    public function getDrawn(): int
    {
        return $this->drawn;
    }

    public function setDrawn(int $drawn): self
    {
        $this->drawn = $drawn;

        return $this;
    }
    
    public function getLost(): int
    {
        return $this->lost;
    }

    public function setLost(int $lost): self
    {
        $this->lost = $lost;

        return $this;
    }
    
    public function getFor(): int
    {
        return $this->for;
    }

    public function setFor(int $for): self
    {
        $this->for = $for;

        return $this;
    }
    
    public function getAgainst(): int
    {
        return $this->against;
    }

    public function setAgainst(int $against): self
    {
        $this->against = $against;

        return $this;
    }
    
    public function getGoalDifference(): int
    {
        return $this->for - $this->against;
    }
    
    public function getPoints(): int
    {
        return $this->points;
    }

    public function setPoints(int $points): self
    {
        $this->points = $points;

        return $this;
    }
}
