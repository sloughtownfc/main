<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Team
 *
 * @ORM\Table(name="teams", uniqueConstraints={@ORM\UniqueConstraint(name="url", columns={"url"})})
 * @ORM\Entity(repositoryClass="App\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 64)
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 32)
     * @ORM\Column(name="url", type="string", length=32, nullable=false)
     */
    private $url;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 32)
     * @ORM\Column(name="short_name", type="string", length=32, nullable=false)
     */
    private $shortName;
    
    /**
     * @var string
     * @Assert\Length(max = 32)
     * @ORM\Column(name="table_name", type="string", length=32, nullable=true)
     */
    private $tableName;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active;

    /**
     * @ORM\OneToMany(targetEntity="LeagueEntry", mappedBy="team")
     */
    private $leagueEntries;
    
    /**
     * @ORM\OneToMany(targetEntity="Competition", mappedBy="team")
     */
    private $competitions;
    
    /**
     * @ORM\OneToMany(targetEntity="Opposition", mappedBy="team")
     */
    private $oppositions;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Player", mappedBy="teams")
     */
    private $players;
    
    /**
     * @ORM\OneToMany(targetEntity="Fixture", mappedBy="team")
     */
    private $fixtures;
    
    public function __construct()
    {
        $this->competitions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leagueEntries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->oppositions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->fixtures = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
    
    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }
    
    public function getTableName(): ?string
    {
        return $this->tableName;
    }

    public function setTableName(string $tableName): self
    {
        $this->tableName = $tableName;

        return $this;
    }
    
    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }
    
    /**
     * @return Collection|Competition[]
     */
    public function getCompetitions(): Collection
    {
        return $this->competitions;
    }

    public function addCompetition(Competition $competition): self
    {
        if (!$this->competitions->contains($competition)) {
            $this->competitions[] = $competition;
        }

        return $this;
    }

    public function removeCompetition(Competition $competition): self
    {
        if ($this->competitions->contains($competition)) {
            $this->competitions->removeElement($competition);
        }

        return $this;
    }
    
    /**
     * @return Collection|LeagueEntry[]
     */
    public function getLeagueEntries(): Collection
    {
        return $this->leagueEntries;
    }

    public function addLeagueEntries(LeagueEntry $leagueEntries): self
    {
        if (!$this->leagueEntries->contains($leagueEntries)) {
            $this->leagueEntries[] = $leagueEntries;
        }

        return $this;
    }

    public function removeLeagueEntries(LeagueEntry $leagueEntries): self
    {
        if ($this->leagueEntries->contains($leagueEntries)) {
            $this->leagueEntries->removeElement($leagueEntries);
        }

        return $this;
    }
    
    /**
     * @return Collection|Opposition[]
     */
    public function getOppositions(): Collection
    {
        return $this->oppositions;
    }

    public function addOpposition(Opposition $opposition): self
    {
        if (!$this->oppositions->contains($opposition)) {
            $this->oppositions[] = $opposition;
        }

        return $this;
    }

    public function removeOpposition(Opposition $opposition): self
    {
        if ($this->oppositions->contains($opposition)) {
            $this->oppositions->removeElement($opposition);
        }

        return $this;
    }
    
    /**
     * @return Collection|Player[]
     */
    public function getPlayers(): Collection
    {
        return $this->players;
    }

    public function addPlayer(Player $player): self
    {
        if (!$this->players->contains($player)) {
            $this->players[] = $player;
            $player->addTeam($this);
        }

        return $this;
    }

    public function removePlayer(Player $player): self
    {
        if ($this->players->contains($player)) {
            $this->players->removeElement($player);
            $player->removeTeam($this);
        }

        return $this;
    }
    
    /**
     * @return Collection|Fixture[]
     */
    public function getFixtures(): Collection
    {
        return $this->fixtures;
    }

    public function addFixture(Fixture $fixture): self
    {
        if (!$this->fixtures->contains($fixture)) {
            $this->fixtures[] = $fixture;
        }

        return $this;
    }

    public function removeFixture(Fixture $fixture): self
    {
        if ($this->fixtures->contains($fixture)) {
            $this->fixtures->removeElement($fixture);
        }

        return $this;
    }
}
