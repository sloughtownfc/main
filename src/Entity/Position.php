<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Position
 *
 * @ORM\Table(name="positions")
 * @ORM\Entity
 */
class Position
{
    const UNKNOWN = "Unknown";
    const MANAGEMENT = "Management";
    const GOALKEEPER = "Goalkeeper";
    const DEFENDER = "Defender";
    const MIDFIELDER = "Midfielder";
    const FORWARD = "Forward";
    
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
