<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Fixture
 *
 * @ORM\Table(name="fixtures", indexes={@ORM\Index(name="fk_fixtures_competition_id_idx", columns={"competition_id"}), @ORM\Index(name="fk_fixtures_opposition_id_idx", columns={"opposition_id"}),@ORM\Index(name="season_fixture_lookup", columns={"team_id", "match_date"})})
 * @ORM\Entity(repositoryClass="App\Repository\FixtureRepository")
 */
class Fixture
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="match_date", type="date", nullable=true)
     */
    private $matchDate;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_home", type="boolean", nullable=true)
     */
    private $isHome;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="kickoff", type="time", nullable=true)
     */
    private $kickoff;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="match_sponsor", type="string", length=128, nullable=true)
     */
    private $matchSponsor;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="ball_sponsor", type="string", length=128, nullable=true)
     */
    private $ballSponsor;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="report_author", type="string", length=64, nullable=true)
     */
    private $reportAuthor;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="report_picture_url", type="string", length=128, nullable=true)
     */
    private $reportPictureUrl;

    /**
     * @var int|null
     *
     * @ORM\Column(name="report_home_score", type="integer", nullable=true)
     */
    private $reportHomeScore;

    /**
     * @var int|null
     *
     * @ORM\Column(name="report_away_score", type="integer", nullable=true)
     */
    private $reportAwayScore;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_home_scorers", type="text", length=65535, nullable=true)
     */
    private $reportHomeScorers;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_away_scorers", type="text", length=65535, nullable=true)
     */
    private $reportAwayScorers;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="report_extra_time", type="boolean", nullable=true)
     */
    private $reportExtraTime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="report_attendance", type="integer", nullable=true)
     */
    private $reportAttendance;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="report_headline", type="string", length=64, nullable=true)
     */
    private $reportHeadline;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_excerpt", type="text", length=65535, nullable=true)
     */
    private $reportExcerpt;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_body", type="text", length=65535, nullable=true)
     */
    private $reportBody;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_friendly_team", type="text", length=65535, nullable=true)
     */
    private $reportFriendlyTeam;

    /**
     * @var \Player
     *
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_mom", referencedColumnName="id")
     * })
     */
    private $reportMom;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="report_away_team", type="text", length=65535, nullable=true)
     */
    private $reportAwayTeam;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="report_flickr_url", type="string", length=128, nullable=true)
     */
    private $reportFlickrUrl;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="report_published", type="datetime", nullable=true)
     */
    private $reportPublished;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="manager_note_author", type="string", length=64, nullable=true)
     */
    private $managerNoteAuthor;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="manager_note_headline", type="string", length=64, nullable=true)
     */
    private $managerNoteHeadline;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="manager_note_excerpt", type="text", length=65535, nullable=true)
     */
    private $managerNoteExcerpt;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="manager_note_body", type="text", length=65535, nullable=true)
     */
    private $managerNoteBody;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="manager_note_picture_url", type="string", length=128, nullable=true)
     */
    private $managerNotePictureUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="manager_note_picture_thumbnail_url", type="string", length=128, nullable=true)
     */
    private $managerNotePictureThumbnailUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="manager_note_caption", type="string", length=128, nullable=true)
     */
    private $managerNoteCaption;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="manager_note_quote", type="text", length=65535, nullable=true)
     */
    private $managerNoteQuote;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="manager_note_quote_credit", type="string", length=64, nullable=true)
     */
    private $managerNoteQuoteCredit;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="manager_note_published", type="datetime", nullable=true)
     */
    private $managerNotePublished;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="coach_departs", type="time", nullable=true)
     */
    private $coachDeparts;

    /**
     * @var string|null
     * @Assert\Length(max = 16)
     * @ORM\Column(name="coach_adult_price", type="string", length=16, nullable=true)
     */
    private $coachAdultPrice;

    /**
     * @var string|null
     * @Assert\Length(max = 16)
     * @ORM\Column(name="coach_reduced_price", type="string", length=16, nullable=true)
     */
    private $coachReducedPrice;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="video_author", type="string", length=64, nullable=true)
     */
    private $videoAuthor;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="video_picture_url", type="string", length=128, nullable=true)
     */
    private $videoPictureUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="video_picture_thumbnail_url", type="string", length=128, nullable=true)
     */
    private $videoPictureThumbnailUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="video_url", type="string", length=128, nullable=true)
     */
    private $videoUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="video_body", type="text", length=65535, nullable=true)
     */
    private $videoBody;

    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="video_headline", type="string", length=64, nullable=true)
     */
    private $videoHeadline;

    /**
     * @var string|null
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="video_excerpt", type="text", length=65535, nullable=true)
     */
    private $videoExcerpt;
    
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="video_published", type="datetime", nullable=true)
     */
    private $videoPublished;

    /**
     * @var string|null
     * @Assert\Length(max = 255)
     * @ORM\Column(name="notification", type="string", length=255, nullable=true)
     */
    private $notification;

    /**
     * @var string|null
     * @Assert\Length(max = 255)
     * @ORM\Column(name="tickets_url", type="string", length=255, nullable=true)
     */
    private $ticketsUrl;
    
    /**
     * @var string|null
     * @Assert\Length(max = 64)
     * @ORM\Column(name="live_stream_id", type="string", length=64, nullable=true)
     */
    private $liveStreamId;
    
    /**
     * @var \Competition
     *
     * @ORM\ManyToOne(targetEntity="Competition", inversedBy="fixtures")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="competition_id", referencedColumnName="id")
     * })
     */
    private $competition;

    /**
     * @var \Opposition
     *
     * @ORM\ManyToOne(targetEntity="Opposition", inversedBy="fixtures")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="opposition_id", referencedColumnName="id")
     * })
     */
    private $opposition;

    /**
     * @ORM\OneToMany(targetEntity="Stats", mappedBy="fixture", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $stats;
    
    /**
     * @var \Team
     *
     * @ORM\ManyToOne(targetEntity="Team", inversedBy="fixtures")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     * })
     */
    private $team;
    
    public function __construct()
    {
        $this->stats = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatchDate(): ?\DateTimeInterface
    {
        return $this->matchDate;
    }

    public function setMatchDate(?\DateTimeInterface $matchDate): self
    {
        $this->matchDate = $matchDate;

        return $this;
    }

    public function getIsHome(): ?bool
    {
        return $this->isHome;
    }

    public function setIsHome(?bool $isHome): self
    {
        $this->isHome = $isHome;

        return $this;
    }

    public function getKickoff(): ?\DateTimeInterface
    {
        return $this->kickoff;
    }

    public function setKickoff(?\DateTimeInterface $kickoff): self
    {
        $this->kickoff = $kickoff;

        return $this;
    }

    public function getMatchSponsor(): ?string
    {
        return $this->matchSponsor;
    }

    public function setMatchSponsor(?string $matchSponsor): self
    {
        $this->matchSponsor = $matchSponsor;

        return $this;
    }

    public function getBallSponsor(): ?string
    {
        return $this->ballSponsor;
    }

    public function setBallSponsor(?string $ballSponsor): self
    {
        $this->ballSponsor = $ballSponsor;

        return $this;
    }

    public function getReportAuthor(): ?string
    {
        return $this->reportAuthor;
    }

    public function setReportAuthor(?string $reportAuthor): self
    {
        $this->reportAuthor = $reportAuthor;

        return $this;
    }

    public function getReportPictureUrl(): ?string
    {
        return $this->reportPictureUrl;
    }

    public function setReportPictureUrl(?string $reportPictureUrl): self
    {
        $this->reportPictureUrl = $reportPictureUrl;

        return $this;
    }

    public function getReportHomeScore(): ?int
    {
        return $this->reportHomeScore;
    }

    public function setReportHomeScore(?int $reportHomeScore): self
    {
        $this->reportHomeScore = $reportHomeScore;

        return $this;
    }

    public function getReportAwayScore(): ?int
    {
        return $this->reportAwayScore;
    }

    public function setReportAwayScore(?int $reportAwayScore): self
    {
        $this->reportAwayScore = $reportAwayScore;

        return $this;
    }

    public function getReportHomeScorers(): ?string
    {
        return $this->reportHomeScorers;
    }

    public function setReportHomeScorers(?string $reportHomeScorers): self
    {
        $this->reportHomeScorers = $reportHomeScorers;

        return $this;
    }

    public function getReportAwayScorers(): ?string
    {
        return $this->reportAwayScorers;
    }

    public function setReportAwayScorers(?string $reportAwayScorers): self
    {
        $this->reportAwayScorers = $reportAwayScorers;

        return $this;
    }

    public function getReportExtraTime(): ?bool
    {
        return $this->reportExtraTime;
    }

    public function setReportExtraTime(?bool $reportExtraTime): self
    {
        $this->reportExtraTime = $reportExtraTime;

        return $this;
    }

    public function getReportAttendance(): ?int
    {
        return $this->reportAttendance;
    }

    public function setReportAttendance(?int $reportAttendance): self
    {
        $this->reportAttendance = $reportAttendance;

        return $this;
    }

    public function getReportHeadline(): ?string
    {
        return $this->reportHeadline;
    }

    public function setReportHeadline(?string $reportHeadline): self
    {
        $this->reportHeadline = $reportHeadline;

        return $this;
    }

    public function getReportExcerpt(): ?string
    {
        return $this->reportExcerpt;
    }

    public function setReportExcerpt(?string $reportExcerpt): self
    {
        $this->reportExcerpt = $reportExcerpt;

        return $this;
    }

    public function getReportBody(): ?string
    {
        return $this->reportBody;
    }

    public function setReportBody(?string $reportBody): self
    {
        $this->reportBody = $reportBody;

        return $this;
    }

    public function getReportFriendlyTeam(): ?string
    {
        return $this->reportFriendlyTeam;
    }

    public function setReportFriendlyTeam(?string $reportFriendlyTeam): self
    {
        $this->reportFriendlyTeam = $reportFriendlyTeam;

        return $this;
    }
    
    public function getReportMom(): ?Player
    {
        return $this->reportMom;
    }

    public function setReportMom(?Player $reportMom): self
    {
        $this->reportMom = $reportMom;

        return $this;
    }

    public function getReportAwayTeam(): ?string
    {
        return $this->reportAwayTeam;
    }

    public function setReportAwayTeam(?string $reportAwayTeam): self
    {
        $this->reportAwayTeam = $reportAwayTeam;

        return $this;
    }

    public function getReportFlickrUrl(): ?string
    {
        return $this->reportFlickrUrl;
    }

    public function setReportFlickrUrl(?string $reportFlickrUrl): self
    {
        $this->reportFlickrUrl = $reportFlickrUrl;

        return $this;
    }

    public function getReportPublishedDateTime(): ?\DateTimeInterface
    {
        return $this->reportPublished;
    }

    public function getReportPublished(): ?bool
    {
        return $this->reportPublished != null;
    }
    
    public function setReportPublished(?bool $reportPublished): self
    {
        if ($reportPublished === true) {
            $this->reportPublished = new \DateTime();
        }
        else {
            $this->reportPublished = null;
        }
        
        return $this;
    }

    public function getManagerNoteAuthor(): ?string
    {
        return $this->managerNoteAuthor;
    }

    public function setManagerNoteAuthor(?string $managerNoteAuthor): self
    {
        $this->managerNoteAuthor = $managerNoteAuthor;

        return $this;
    }

    public function getManagerNoteHeadline(): ?string
    {
        return $this->managerNoteHeadline;
    }

    public function setManagerNoteHeadline(?string $managerNoteHeadline): self
    {
        $this->managerNoteHeadline = $managerNoteHeadline;

        return $this;
    }

    public function getManagerNoteExcerpt(): ?string
    {
        return $this->managerNoteExcerpt;
    }

    public function setManagerNoteExcerpt(?string $managerNoteExcerpt): self
    {
        $this->managerNoteExcerpt = $managerNoteExcerpt;

        return $this;
    }

    public function getManagerNoteBody(): ?string
    {
        return $this->managerNoteBody;
    }

    public function setManagerNoteBody(?string $managerNoteBody): self
    {
        $this->managerNoteBody = $managerNoteBody;

        return $this;
    }

    public function getManagerNotePictureUrl(): ?string
    {
        return $this->managerNotePictureUrl;
    }

    public function setManagerNotePictureUrl(?string $managerNotePictureUrl): self
    {
        $this->managerNotePictureUrl = $managerNotePictureUrl;

        return $this;
    }

    public function getManagerNotePictureThumbnailUrl(): ?string
    {
        return $this->managerNotePictureThumbnailUrl;
    }

    public function setManagerNotePictureThumbnailUrl(?string $managerNotePictureThumbnailUrl): self
    {
        $this->managerNotePictureThumbnailUrl = $managerNotePictureThumbnailUrl;

        return $this;
    }

    public function getManagerNoteCaption(): ?string
    {
        return $this->managerNoteCaption;
    }

    public function setManagerNoteCaption(?string $managerNoteCaption): self
    {
        $this->managerNoteCaption = $managerNoteCaption;

        return $this;
    }

    public function getManagerNoteQuote(): ?string
    {
        return $this->managerNoteQuote;
    }

    public function setManagerNoteQuote(?string $managerNoteQuote): self
    {
        $this->managerNoteQuote = $managerNoteQuote;

        return $this;
    }

    public function getManagerNoteQuoteCredit(): ?string
    {
        return $this->managerNoteQuoteCredit;
    }

    public function setManagerNoteQuoteCredit(?string $managerNoteQuoteCredit): self
    {
        $this->managerNoteQuoteCredit = $managerNoteQuoteCredit;

        return $this;
    }

    public function getManagerNotePublishedDateTime(): ?\DateTimeInterface
    {
        return $this->managerNotePublished;
    }
    
    public function getManagerNotePublished(): ?bool
    {
        return $this->managerNotePublished != null;
    }

    public function setManagerNotePublished(?bool $managerNotePublished): self
    {
        if ($managerNotePublished === true) {
            $this->managerNotePublished = new \DateTime();
        }
        else {
            $this->managerNotePublished = null;
        }

        return $this;
    }

    public function getCoachDeparts(): ?\DateTimeInterface
    {
        return $this->coachDeparts;
    }

    public function setCoachDeparts(?\DateTimeInterface $coachDeparts): self
    {
        $this->coachDeparts = $coachDeparts;

        return $this;
    }

    public function getCoachAdultPrice(): ?string
    {
        return $this->coachAdultPrice;
    }

    public function setCoachAdultPrice(?string $coachAdultPrice): self
    {
        $this->coachAdultPrice = $coachAdultPrice;

        return $this;
    }

    public function getCoachReducedPrice(): ?string
    {
        return $this->coachReducedPrice;
    }

    public function setCoachReducedPrice(?string $coachReducedPrice): self
    {
        $this->coachReducedPrice = $coachReducedPrice;

        return $this;
    }

    public function getVideoAuthor(): ?string
    {
        return $this->videoAuthor;
    }

    public function setVideoAuthor(?string $videoAuthor): self
    {
        $this->videoAuthor = $videoAuthor;

        return $this;
    }

    public function getVideoPictureUrl(): ?string
    {
        return $this->videoPictureUrl;
    }

    public function setVideoPictureUrl(?string $videoPictureUrl): self
    {
        $this->videoPictureUrl = $videoPictureUrl;

        return $this;
    }

    public function getVideoPictureThumbnailUrl(): ?string
    {
        return $this->videoPictureThumbnailUrl;
    }

    public function setVideoPictureThumbnailUrl(?string $videoPictureThumbnailUrl): self
    {
        $this->videoPictureThumbnailUrl = $videoPictureThumbnailUrl;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(?string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getVideoBody(): ?string
    {
        return $this->videoBody;
    }

    public function setVideoBody(?string $videoBody): self
    {
        $this->videoBody = $videoBody;

        return $this;
    }

    public function getVideoHeadline(): ?string
    {
        return $this->videoHeadline;
    }

    public function setVideoHeadline(?string $videoHeadline): self
    {
        $this->videoHeadline = $videoHeadline;

        return $this;
    }

    public function getVideoExcerpt(): ?string
    {
        return $this->videoExcerpt;
    }

    public function setVideoExcerpt(?string $videoExcerpt): self
    {
        $this->videoExcerpt = $videoExcerpt;

        return $this;
    }
    
    public function getVideoPublishedDateTime(): ?\DateTimeInterface
    {
        return $this->videoPublished;
    }

    public function getVideoPublished(): ?bool
    {
        return $this->videoPublished != null;
    }
    
    public function setVideoPublished(?bool $videoPublished): self
    {
        if ($videoPublished === true) {
            $this->videoPublished = new \DateTime();
        }
        else {
            $this->videoPublished = null;
        }
        
        return $this;
    }

    public function getTicketsUrl(): ?string
    {
        return $this->ticketsUrl;
    }

    public function setTicketsUrl(?string $ticketsUrl): self
    {
        $this->ticketsUrl = $ticketsUrl;

        return $this;
    }
        
    public function getLiveStreamId(): ?string
    {
        return $this->liveStreamId;
    }

    public function setLiveStreamId(?string $liveStreamId): self
    {
        $this->liveStreamId = $liveStreamId;

        return $this;
    }
    
    
    public function getNotification(): ?string
    {
        return $this->notification;
    }

    public function setNotification(?string $notification): self
    {
        $this->notification = $notification;

        return $this;
    }
    
    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(?Competition $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    public function getOpposition(): ?Opposition
    {
        return $this->opposition;
    }

    public function setOpposition(?Opposition $opposition): self
    {
        $this->opposition = $opposition;

        return $this;
    }

    /**
     * @return Collection|Stats[]
     */
    public function getStats(): Collection
    {
        return $this->stats;
    }

    public function addStat(Stats $stats): self
    {
        if (!$this->stats->contains($stats)) {
            $this->stats[] = $stats;
            $stats->setFixture($this);
        }

        return $this;
    }

    public function removeStat(Stats $stats): self
    {
        if ($this->stats->contains($stats)) {
            $this->stats->removeElement($stats);
            
            $stats->setFixture(null);
            $stats->setPlayer(null);
        }

        return $this;
    }
    
    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }
}
