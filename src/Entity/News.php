<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="category", type="string", length=128, nullable=false)
     */
    private $category;
    
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="author", type="text", length=65535, nullable=false)
     */
    private $author;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="headline", type="text", length=65535, nullable=false)
     */
    private $headline;

    /**
     * @var \DateTime
     * @Assert\NotBlank()
     * @ORM\Column(name="publish_date", type="date", nullable=false)
     */
    private $publishDate;

    /**
     * @var \DateTime
     * @Assert\NotBlank()
     * @ORM\Column(name="publish_time", type="time", nullable=false)
     */
    private $publishTime;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="excerpt", type="text", length=65535, nullable=false)
     */
    private $excerpt;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="body", type="text", length=65535, nullable=false)
     */
    private $body;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="picture_url", type="string", length=128, nullable=false)
     */
    private $pictureUrl;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 128)
     * @ORM\Column(name="small_picture_url", type="string", length=128, nullable=false)
     */
    private $smallPictureUrl;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="caption", type="text", length=65535, nullable=false)
     */
    private $caption;

    /**
     * @var string
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="quote", type="text", length=65535, nullable=true)
     */
    private $quote;

    /**
     * @var string
     * @Assert\Length(max = 65535)
     * @ORM\Column(name="quote_by", type="text", length=65535, nullable=true)
     */
    private $quoteBy;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="video_url", type="string", length=128, nullable=true)
     */
    private $videoUrl;

    /**
     * @var string|null
     * @Assert\Length(max = 128)
     * @ORM\Column(name="audio_url", type="string", length=150, nullable=true)
     */
    private $audioUrl;
    
    public static $categories = [
        'General' => 'general',
        'Mens' => 'mens',
        'Ladies' => 'ladies',
        'EDS' => 'eds',
        'Juniors' => 'juniors',
        'Academy' => 'academy',
        'Video' => 'video'
    ];
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }
    
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    public function setHeadline(string $headline): self
    {
        $this->headline = $headline;

        return $this;
    }

    public function getUrlHeadline(): string
    {
        return empty($this->headline) ? '' : str_replace(' ', '-', $this->headline);
    }
    
    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publishDate;
    }

    public function setPublishDate(\DateTimeInterface $publishDate): self
    {
        $this->publishDate = $publishDate;

        return $this;
    }

    public function getPublishTime(): ?\DateTimeInterface
    {
        return $this->publishTime;
    }

    public function setPublishTime(\DateTimeInterface $publishTime): self
    {
        $this->publishTime = $publishTime;

        return $this;
    }

    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    public function setExcerpt(string $excerpt): self
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;

        return $this;
    }

    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    public function setPictureUrl(string $pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    public function getSmallPictureUrl(): ?string
    {
        return $this->smallPictureUrl;
    }

    public function setSmallPictureUrl(string $smallPictureUrl): self
    {
        $this->smallPictureUrl = $smallPictureUrl;

        return $this;
    }

    public function getCaption(): ?string
    {
        return $this->caption;
    }

    public function setCaption(string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    public function getQuote(): ?string
    {
        return $this->quote;
    }

    public function setQuote(string $quote): self
    {
        $this->quote = $quote;

        return $this;
    }

    public function getQuoteBy(): ?string
    {
        return $this->quoteBy;
    }

    public function setQuoteBy(string $quoteBy): self
    {
        $this->quoteBy = $quoteBy;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setVideoUrl(?string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getAudioUrl(): ?string
    {
        return $this->audioUrl;
    }

    public function setAudioUrl(?string $audioUrl): self
    {
        $this->audioUrl = $audioUrl;

        return $this;
    }
    
    public function getPublished(): bool
    {
        return new \DateTime() >= \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $this->publishDate->format('Y-m-d'), $this->publishTime->format('H:i:s')));
    }
}
