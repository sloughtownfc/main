<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Notification;

class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    /**
     * @return Notification[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('n')
                   ->orderBy('n.id', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
    
    /**
     * @return Notification
     */
    public function getCurrent(): ?Notification
    {
        $now = new \DateTime();

        return $this->createQueryBuilder('n')
                    ->andWhere('n.startDate <= :now')
                    ->andWhere('(n.endDate IS NULL OR n.endDate >= :now)')
                    ->setParameter('now', $now->format('Y-m-d H:m'))
                    ->orderBy('n.startDate', 'ASC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}
        
