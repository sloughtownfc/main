<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;

class ArchiveRepository
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * @return array
     */
    public function getAutoComplete($term): array
    {       
        $sql = 'SELECT `name` FROM (
                        SELECT full_name as `name` FROM players WHERE full_name LIKE ?
                        UNION
                        SELECT `name` FROM oppositions WHERE `name` LIKE ?
                ) a
                ORDER BY `name`
                LIMIT 10';
        
        $s = $this->em->getConnection()->prepare($sql);
        $s->bindValue(1, '%' . $term . '%');
        $s->bindValue(2, '%' . $term . '%');
        $s->execute();

        return array_column($s->fetchAll(), 'name');
    }
    
    public function search($term): array
    {
        $sql = 'SELECT id, full_name as `name` FROM players WHERE full_name LIKE ?';
        $s = $this->em->getConnection()->prepare($sql);
        $s->bindValue(1, '%' . $term . '%');
        $s->execute();
        
        $result['players'] = $s->fetchAll();
        
        $sql = 'SELECT id, `name` FROM oppositions WHERE `name` LIKE ?';
        $s = $this->em->getConnection()->prepare($sql);
        $s->bindValue(1, '%' . $term . '%');
        $s->execute();
        
        $result['oppositions'] = $s->fetchAll();
        
        return $result;
    }
}
        
