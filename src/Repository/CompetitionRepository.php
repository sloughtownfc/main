<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Competition;
use App\Entity\Team;

class CompetitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Competition::class);
    }

    /**
     * @return Competition[]
     */
    public function findAllByTeamAndPage(Team $team, $page, $count): array
    {
        $qb = $this->createQueryBuilder('c')
                   ->where('c.team = :team')
                   ->setParameter('team', $team)
                   ->orderBy('c.id', 'ASC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(Team $team): int
    {
        return $this->count(['team' => $team]);
    }
    
    /**
     * @return Competition
     */
    public function getCompetitionAndFixturesById($id): Competition
    {
        return $this->createQueryBuilder('c')
                    ->innerJoin('c.fixtures', 'f')
                    ->addSelect('f')
                    ->innerJoin('f.opposition', 'o')
                    ->addSelect('o')
                    ->andWhere('c.id = :id')
                    ->addOrderBy('f.matchDate', 'DESC')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /**
     * @return Competition[]
     */
    public function getPartialByTeam($teamId): array
    {
        return $this->getPartialByTeamQb($teamId)
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return QueryBuilder
     */
    public function getPartialByTeamQb($teamId): QueryBuilder
    {
        return $this->createQueryBuilder('c')
                    ->select('partial c.{ id, name }')
                    ->where('c.team = :team')
                    ->setParameter('team', $teamId)
                    ->addOrderBy('c.name', 'ASC');
    }
}
        
