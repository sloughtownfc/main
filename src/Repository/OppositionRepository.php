<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Opposition;
use App\Entity\Season;
use App\Entity\Team;

class OppositionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Opposition::class);
    }

    /**
     * @return Opposition[]
     */
    public function findAllByTeamAndPage(Team $team, $page, $count): array
    {
        $qb = $this->createQueryBuilder('o')
                   ->where('o.team = :team')
                   ->setParameter('team', $team)
                   ->orderBy('o.name', 'ASC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(Team $team): int
    {
        return $this->count(['team' => $team]);
    }
    
    /**
     * @return Opposition
     */
    public function getOppositionAndFixturesById($id): ?Opposition
    {
        return $this->createQueryBuilder('o')
                    ->leftJoin('o.fixtures', 'f', 'WITH', 'f.matchDate IS NOT NULL AND (f.reportPublished IS NOT NULL OR f.reportPublished <> 0)')
                    ->addSelect('partial f.{ id, matchDate, isHome, reportHomeScore, reportAwayScore }')
                    ->leftJoin('f.competition', 'c')
                    ->addSelect('partial c.{ id, name }')
                    ->andWhere('o.id = :id')
                    ->addOrderBy('f.matchDate', 'DESC')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /**
     * @return Opposition[]
     */
    public function findAllAwayBySeason(?Season $season): array
    {
        if (!$season) {
            return [];
        }
        
        return $this->createQueryBuilder('o')
                    ->select('partial o.{ id, name, groundsVenue, groundsAddress, groundsLat, groundsLng }')
                    ->innerJoin('o.fixtures', 'f')
                    ->andWhere('f.matchDate BETWEEN :start AND :end')
                    ->andWhere('f.isHome = 0')
                    ->setParameter('start', $season->getStartDate())
                    ->setParameter('end', $season->getEndDate())
                    ->orderBy('o.name', 'ASC')
                    ->distinct()
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return array
     */
    public function getStatistics(Opposition $opposition): array
    {       
        $sql = 'SELECT
                SUM(CASE WHEN is_home = 1 THEN 1 ELSE 0 END) as home_played,
                SUM(CASE WHEN is_home = 0 THEN 1 ELSE 0 END) as away_played,
                SUM(CASE WHEN report_home_score > report_away_score AND is_home = 1 THEN 1 ELSE 0 END) as home_win,
                SUM(CASE WHEN report_home_score < report_away_score AND is_home = 0 THEN 1 ELSE 0 END) as away_win,
                SUM(CASE WHEN report_home_score < report_away_score AND is_home = 1 THEN 1 ELSE 0 END) as home_loss,
                SUM(CASE WHEN report_home_score > report_away_score AND is_home = 0 THEN 1 ELSE 0 END) as away_loss,
                SUM(CASE WHEN report_home_score = report_away_score AND is_home = 1 THEN 1 ELSE 0 END) as home_draw,
                SUM(CASE WHEN report_home_score = report_away_score AND is_home = 0 THEN 1 ELSE 0 END) as away_draw,
                SUM(CASE WHEN is_home = 1 THEN report_home_score ELSE 0 END) as home_scored,
                SUM(CASE WHEN is_home = 0 THEN report_away_score ELSE 0 END) as away_scored,
                SUM(CASE WHEN is_home = 1 THEN report_away_score ELSE 0 END) as home_conceded,
                SUM(CASE WHEN is_home = 0 THEN report_home_score ELSE 0 END) as away_conceded
                FROM fixtures
                WHERE opposition_id = :opposition AND report_published IS NOT NULL;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':opposition', $opposition->getId());
        $s->execute();
        $result = $s->fetch();
        $s->closeCursor();
        
        return $result;
    }
    
    /**
     * @return Opposition[]
     */
    public function getPartialByTeam($teamId): array
    {
        return $this->getPartialByTeamQb($teamId)
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return QueryBuilder
     */
    public function getPartialByTeamQb($teamId): QueryBuilder
    {
        return $this->createQueryBuilder('o')
                    ->select('partial o.{ id, name }')
                    ->where('o.team = :team')
                    ->setParameter('team', $teamId)
                    ->addOrderBy('o.name', 'ASC');
    }
}
        
