<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\LeagueEntry;
use App\Entity\Team;

class LeagueEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeagueEntry::class);
    }

    /**
     * @return LeagueEntry
     */
    public function getPosition(Team $team, $type): ?LeagueEntry
    {
        return $this->createQueryBuilder('le')
                    ->select('partial le.{ position, team, type, name, points }')
                    ->where('le.team = :team')
                    ->andWhere('le.type = :type')
                    ->andWhere('le.name = :name')
                    ->setParameter('team', $team)
                    ->setParameter('type', $type)
                    ->setParameter('name', $team->getTableName())
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    public function getSnapshot(Team $team, $type, $count = 3): array
    {
        $table = $this->createQueryBuilder('le')
            ->select('le')
            ->where('le.team = :team')
            ->andWhere('le.type = :type')
            ->setParameter('team', $team)
            ->setParameter('type', $type)
            ->addOrderBy('le.position', 'ASC')
            ->getQuery()
            ->execute();
        
        $entries = count($table);
        
        if ($entries <= $count) {
            return $table;
        }
            
        $idx = -1;
        
        foreach ($table as $key => $value) {
            if ($value->getName() === $team->getTableName()) {
                $idx = $key;
                break;
            }
        }
        
        $lastIdx = $entries - 1;
        
        switch($idx) {
            case -1:
                return [];
            case 0:
                return array_slice($table, 0, $count);
            case $lastIdx:
                return array_slice($table, -$count);
            default:
                return array_slice($table, $idx - floor($count / 2), $count);
        }
    }
}
        
