<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @return User[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('u')
                   ->leftJoin('u.roles', 'r')
                   ->addSelect('partial r.{ id, name }')
                   ->addOrderBy('u.id', 'ASC')
                   ->addOrderBy('r.name', 'ASC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
}
        
