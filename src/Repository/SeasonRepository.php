<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Season;
use App\Entity\Team;

class SeasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Season::class);
    }

    /**
     * @return Season[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('s')
                   ->orderBy('s.endDate', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
    
    /*
     * @return Season[]
     */
    public function getNewsList(): array
    {
        return $this->createQueryBuilder('s')
                    ->where('s.startDate > \'2007-06-01\'')
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return Season
     */
    public function getCurrent(): ?Season
    {
        $now = new \DateTime();

        return $this->createQueryBuilder('s')
                    ->andWhere('s.startDate <= :now')
                    ->andWhere('s.endDate >= :now')
                    ->setParameter('now', $now->format('Y-m-d'))
                    ->orderBy('s.endDate', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /*
     * @return Season[]
     */
    public function getSeasonsForTeam(Team $team): array
    {
        return $this->createQueryBuilder('s')
                  ->select('s')
                  ->innerJoin('App\Entity\Fixture', 'f', Join::WITH , 'f.matchDate BETWEEN s.startDate AND s.endDate AND f.team = :team')
                  ->setParameter('team', $team)
                  ->distinct()
                  ->getQuery()
                  ->execute();
    }
}
        
