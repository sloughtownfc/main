<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\News;
use App\Entity\Season;

class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    /**
     * @return News[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('n')
                   ->orderBy('n.id', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * @return News[]
     */
    public function findAllBySeason(Season $season, $category): array
    { 
        $qb = $this->createQueryBuilder('n')
                   ->andWhere('n.publishDate BETWEEN :start AND :end')
                   ->andWhere('CONCAT(n.publishDate, \' \', n.publishTime) <= :currdate')
                   ->setParameter('start', $season->getStartDate())
                   ->setParameter('end', $season->getEndDate())
                   ->setParameter('currdate', date('Y-m-d H:i:s'))
                   ->addOrderBy('n.publishDate', 'DESC')
                   ->addOrderBy('n.publishTime',  'DESC');
        
        if ($category) {
            $qb = $qb->andWhere('n.category = :category')
                     ->setParameter('category', $category);
        }
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * @return News
     */
    public function getArticle($id): ?News
    {
        return $this->createQueryBuilder('n')
                    ->andWhere('n.id = :id')
                    ->andWhere('CONCAT(n.publishDate, \' \', n.publishTime) <= :currdate')
                    ->setParameter('id', $id)
                    ->setParameter('currdate', date('Y-m-d H:i:s'))
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
}
        
