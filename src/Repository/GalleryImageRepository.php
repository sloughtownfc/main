<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\GalleryImage;

class GalleryImageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GalleryImage::class);
    }

    public function getCount($tag = null): int
    {
        if (empty($tag)) {
            return $this->count([]);
        }

        return count($this->getImageIdsForTag($tag));
    }
    
    /**
     * @return GalleryImage[]
     */
    public function findAllByPage($page, $count, $tag = null): array
    {
        $qb = $this->createQueryBuilder('gi')
                   ->select('partial gi.{ id, filename, thumbnail }')
                   ->orderBy('gi.id', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        if (!empty($tag)) {
            $ids = $this->getImageIdsForTag($tag);
            
            $qb->where('gi.id in (:ids)')
               ->setParameter('ids', $ids);
        }
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * @param string $md5
     * 
     * @return bool
     */
    public function isDuplicate(string $md5): bool
    {
        return $this->count(['originalMd5' => $md5]) > 0;
    }
    
    private function getImageIdsForTag($tag): array
    {
        $res = $this->createQueryBuilder('gi')
                    ->select('gi.id')
                    ->innerJoin('gi.tags', 't')
                    ->where('t.name LIKE :tag')
                    ->setParameter('tag', "%$tag%")
                    ->getQuery()
                    ->getArrayResult();
                   
        return array_column($res, 'id');
    }
}
        
