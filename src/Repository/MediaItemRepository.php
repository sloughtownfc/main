<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\MediaItem;

class MediaItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MediaItem::class);
    }

    /**
     * @return MediaItem[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('m')
                   ->orderBy('m.id', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
    
    /**
     * @return MediaItem[]
     */
    public function getLatest(int $count): array
    {
        return $this->createQueryBuilder('m')
                    ->orderBy('m.id', 'DESC')
                    ->setMaxResults($count)
                    ->getQuery()
                    ->execute();
    }
}
        
