<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;

class HomepageRepository
{
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }
    
    /**
     * @return array
     */
    public function getArticles($count = 6): array
    {
        $date = date('Y-m-d H:i:s');
        
        $sql = 'SELECT * FROM (
                    SELECT id, \'news\' as `type`, headline, excerpt, picture_url, CONCAT(publish_date, \' \', publish_time) as published
                    FROM news
                    WHERE publish_date IS NOT NULL AND publish_time IS NOT NULL AND CONCAT(publish_date, \' \', publish_time) <= \''. $date . '\'
                    ORDER BY published DESC
                    LIMIT '.$count.'
                ) a
                UNION
                SELECT * FROM (
                    SELECT id, \'manager\' as `type`, manager_note_headline as headline, manager_note_excerpt as excerpt, manager_note_picture_url as picture_url, manager_note_published as published
                    FROM fixtures
                    WHERE manager_note_published IS NOT NULL
                    ORDER BY manager_note_published DESC
                    LIMIT '.$count.'
                ) b
                UNION
                SELECT * FROM (
                    SELECT id, \'report\' as `type`, report_headline as headline, report_excerpt as excerpt, report_picture_url as picture_url, report_published as published
                    FROM fixtures
                    WHERE report_published IS NOT NULL
                    ORDER BY report_published DESC
                    LIMIT '.$count.'
                ) c
                UNION
                SELECT * FROM (
                    SELECT id, \'video\' as `type`, video_headline as headline, video_excerpt as excerpt, video_picture_url as picture_url, video_published as published
                    FROM fixtures
                    WHERE video_published IS NOT NULL
                    ORDER BY video_published DESC
                    LIMIT '.$count.'
                ) d
                ORDER BY published DESC
                LIMIT '.$count.';';
        
        $s = $this->em->getConnection()->prepare($sql);
        $s->execute();
        
        return $s->fetchAll();
    }
    
    /**
     * @return array
     */
    public function getVideos($count = 8): array
    {
        $date = date('Y-m-d H:i:s');
        
        $sql = 'SELECT * FROM (
                    SELECT id, \'news\' as `type`, headline, excerpt, picture_url, CONCAT(publish_date, \' \', publish_time) as published
                    FROM news
                    WHERE category = \'video\' AND publish_date IS NOT NULL AND publish_time IS NOT NULL AND CONCAT(publish_date, \' \', publish_time) <= \''. $date . '\'
                    ORDER BY published DESC
                    LIMIT '.$count.'
                ) a
                UNION           
                SELECT * FROM (
                    SELECT id, \'video\' as `type`, video_headline as headline, video_excerpt as excerpt, video_picture_url as picture_url, video_published as published
                    FROM fixtures
                    WHERE video_published IS NOT NULL
                    ORDER BY video_published DESC
                    LIMIT '.$count.'
                ) d
                ORDER BY published DESC
                LIMIT '.$count.';';
        
        $s = $this->em->getConnection()->prepare($sql);
        $s->execute();
        
        return $s->fetchAll();
    }
}
        
