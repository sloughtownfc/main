<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Player;
use App\Entity\Season;
use App\Entity\Stats;
use App\Entity\Team;

class PlayerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Player::class);
    }

    /**
     * @return Player[]
     */
    public function findAllByTeamAndPage(Team $team, $page, $count, $query): array
    {
        $qb = $this->createQueryBuilder('p')
                   ->innerJoin('p.position', 'pos')
                   ->innerJoin('p.teams', 't')
                   ->where('t = :team')
                   ->setParameter('team', $team)
                   ->addOrderBy('p.lastName', 'ASC')
                   ->addOrderBy('p.firstName', 'ASC')
                   ->setMaxResults($count);
        
        if (!empty($query)) {
            $qb->andWhere('CONCAT(p.firstName, \' \', p.lastName) LIKE :query')
               ->setParameter('query', '%' . $query . '%');
        }
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(Team $team, $query): int
    {
        if (!empty($query)) {
            $qb = $this->createQueryBuilder('p')
                       ->select('count(p.id)')
                       ->innerJoin('p.teams', 't')
                       ->where('t = :team')
                       ->andWhere('CONCAT(p.firstName, \' \', p.lastName) LIKE :query')
                       ->setParameter('team', $team)
                       ->setParameter('query', '%' . $query . '%');
            
            return $qb->getQuery()->getSingleScalarResult();
        }
        else {
            $qb = $this->createQueryBuilder('p')
                       ->select('count(p.id)')
                       ->innerJoin('p.teams', 't')
                       ->where('t = :team')
                       ->setParameter('team', $team);
            
            return $qb->getQuery()->getSingleScalarResult();
        }
    }
    
    /**
     * @return Player[]
     */
    public function getAllOrderByName(): array
    {
        return $this->getAllOrderByNameQb()->getQuery()->execute();
    }
    
    /**
     * @return QueryBuilder
     */
    public function getAllOrderByNameQb(): QueryBuilder
    {
        return $this->createQueryBuilder('p')
                    ->select('partial p.{ id, firstName, lastName }')
                    ->addOrderBy('p.lastName', 'ASC')
                    ->addOrderBy('p.firstName', 'ASC');
    }
    
    /**
     * @return array
     */
    public function getAllTimeTop20(): array
    {
        $teamRepo = $this->getEntityManager()->getRepository(Team::class);
        $team = $teamRepo->findOneBy(['url' => 'mens']);
        
        $sql = 'SELECT full_name, SUM(goals) as scored, COUNT(*) as appearances
                FROM players p
                INNER JOIN stats s ON p.id = s.player_id
                INNER JOIN fixtures f ON s.fixture_Id = f.id
                INNER JOIN players_teams pt ON p.id = pt.player_id
                WHERE s.appearance = 1 AND f.team_id = :teamId AND pt.team_id = :teamId
                GROUP BY full_name
                ORDER BY scored DESC
                LIMIT 20;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':teamId', $team->getId());
        $s->execute();
        $goals = $s->fetchAll();
        $s->closeCursor();
        
        $sql = 'SELECT full_name, COUNT(*) as appearances
                FROM players p
                INNER JOIN stats s ON p.id = s.player_id
                INNER JOIN fixtures f ON s.fixture_Id = f.id
                INNER JOIN players_teams pt ON p.id = pt.player_id
                WHERE s.appearance = 1 AND f.team_id = :teamId AND pt.team_id = :teamId
                GROUP BY full_name
                ORDER BY appearances DESC
                LIMIT 20;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':teamId', $team->getId());
        $s->execute();
        $appearances = $s->fetchAll();
        $s->closeCursor();
        
        return [
            'goals' => $goals,
            'appearances' => $appearances
        ];
    }
    
    /**
     * @return array
     */
    public function getCurrentSquadByTeam(Team $team): array
    {
        $season = $this->getEntityManager()->getRepository(Season::class)->getCurrent();
        if (!$season) {
            return [];
        }
        
        $sql = 'SELECT p.id, CONCAT(p.first_name, \' \', p.last_name) as name, CONCAT(p.first_name, \'-\', p.last_name) as urlname, squad_number as squadNumber,
                pos.name as position, p.picture_url as pictureUrl, IFNULL(SUM(s.appearance), 0) as appearances, IFNULL(SUM(s.goals), 0) as goals,
                SUM(CASE WHEN s.bookings = 1 THEN 1 ELSE 0 END) as yellows, SUM(CASE WHEN s.bookings = 2 THEN 1 ELSE 0 END) as reds,
                IFNULL(fcs.clean_sheets, 0) as cleanSheets, on_loan as onLoan, title
                FROM players p
                INNER JOIN positions pos ON p.position_id = pos.id
                INNER JOIN players_teams pt ON p.id = pt.player_id
                LEFT OUTER JOIN (
                    SELECT s.player_id, s.appearance, s.goals, s.bookings
                    FROM stats s
                    INNER JOIN fixtures f ON s.fixture_id = f.id
                    WHERE f.team_id = :teamId AND f.match_date BETWEEN :start AND :end AND s.appearance = 1
                ) s ON p.id = s.player_id
                LEFT OUTER JOIN (
                    SELECT s.player_id, COUNT(f.id) as clean_sheets
                    FROM fixtures f
                    INNER JOIN stats s ON f.id = s.fixture_id
                    WHERE f.team_id = :teamId AND f.match_date BETWEEN :start AND :end AND s.appearance = 1
                    AND ((f.is_home = 1 AND IFNULL(f.report_away_score, 0) = 0) OR (f.is_home = 0 AND IFNULL(f.report_home_score, 0) = 0))
                    GROUP BY s.player_id
                ) fcs ON p.id = fcs.player_id
                WHERE p.active = 1 and pt.team_id = :teamId
                GROUP BY p.id, p.first_name, p.last_name, pos.name, p.picture_url, p.squad_number, fcs.clean_sheets
                ORDER BY p.order_by desc, pos.id, p.first_name, p.last_name;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':start', $season->getStartDate()->format('Y-m-d'));
        $s->bindValue(':end', $season->getEndDate()->format('Y-m-d'));
        $s->bindValue(':teamId', $team->getId());
        $s->execute();
        
        return $s->fetchAll();
    }
    
    /**
     * @return Player[]
     */
    public function getPartialByTeam($teamId): array
    {
        return $this->getPartialByTeamQb($teamId)   
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return QueryBuilder
     */
    public function getPartialByTeamQb($teamId): QueryBuilder
    {
        return $this->createQueryBuilder('p')
                    ->select('partial p.{ id, firstName, lastName }')
                    ->innerJoin('p.teams', 't')
                    ->where('t.id = :team')
                    ->setParameter('team', $teamId)    
                    ->addOrderBy('p.lastName', 'ASC')
                    ->addOrderBy('p.firstName', 'ASC');
    }
    
    /**
     * @return Player
     */
    public function getPlayerAndPositionAndStatsAndFixturesAndOppositionAndCompetitionById($id): ?Player
    {
        return $this->createQueryBuilder('p')
                    ->innerJoin('p.position', 'pos')
                    ->addSelect('pos')
                    ->leftJoin('p.stats', 's')
                    ->addSelect('partial s.{ id }')
                    ->leftJoin('s.fixture', 'f')
                    ->addSelect('partial f.{ id, matchDate, isHome }')
                    ->leftJoin('f.competition', 'c')
                    ->addSelect('partial c.{ id, name }')
                    ->leftJoin('f.opposition', 'o')
                    ->addSelect('partial o.{ id, name, logo }')
                    ->andWhere('p.id = :id')
                    ->orderBy('f.matchDate', 'DESC')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /*
     * @return array
     */
    public function getPlayerNotifications(): array
    {
        $sql = 'SELECT a.first_name, a.last_name, (a.played + 1) as next_appearance
                FROM (
                        SELECT p.id, p.first_name, p.last_name, COUNT(*) as played
                        FROM fixtures f
                        INNER JOIN stats s ON f.id = s.fixture_id
                        INNER JOIN players p ON s.player_id = p.id
                        WHERE p.active = 1 AND s.appearance = 1
                        GROUP BY p.id, p.first_name, p.last_name
                ) a
                WHERE a.played % 10 = 9 AND (a.played + 1) % 50 = 0;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->execute();
        
        return $s->fetchAll();
    }
    
    /**
     * @return Player
     */
    public function getPlayerById($id): ?Player
    {
        return $this->createQueryBuilder('p')
                    ->innerJoin('p.position', 'pos')
                    ->addSelect('pos')
                    ->andWhere('p.id = :id')
                    ->setParameter('id', $id)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
    
    /**
     * @return array
     */
    public function getStatistics(Player $player): array
    {       
        $sql = 'SELECT t.short_name as team, IFNULL(SUM(s.goals), 0) as goals, COUNT(s.id) as appearances
                FROM stats s
                INNER JOIN fixtures f ON s.fixture_id = f.id
                INNER JOIN teams t ON f.team_id = t.id
                WHERE s.player_id = :player AND s.appearance = 1
                GROUP BY t.short_name;';
        
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':player', $player->getId());
        $s->execute();
        $goalsAppearances = $s->fetchAll();
        $s->closeCursor();
        
        $goalsAppearances =  array_reduce($goalsAppearances, function($accumulator, $item){
            $index = $item['team'];

            $accumulator[$index] = [
              'goals' => $item['goals'],
              'appearances' => $item['appearances']
            ];

            return $accumulator;
            },
            []
        );
        
        $sql = 'SELECT t.short_name as team, COUNT(f.id) as clean_sheets
                FROM fixtures f
                INNER JOIN stats s ON f.id = s.fixture_id
                INNER JOIN teams t ON f.team_id = t.id
                WHERE s.player_id = :player AND s.appearance = 1
                AND ((f.is_home = 1 AND IFNULL(f.report_away_score, 0) = 0) OR (f.is_home = 0 AND IFNULL(f.report_home_score, 0) = 0))
                GROUP BY t.short_name;';
                
        $s = $this->getEntityManager()->getConnection()->prepare($sql);
        $s->bindValue(':player', $player->getId());
        $s->execute();
        $cleanSheets = $s->fetchAll();
        $s->closeCursor();
        
        $cleanSheets = array_reduce($cleanSheets, function($accumulator, $item) {
            $index = $item['team'];

            $accumulator[$index] = [
              'cleanSheets' => $item['clean_sheets']
            ];

            return $accumulator;
            },
            []
        );
        
        $results = array_merge_recursive($goalsAppearances, $cleanSheets);
        $teamRepo = $this->getEntityManager()->getRepository(Team::class);
        $orderedTeams = $teamRepo->findBy([], ['id' => 'asc']);
        
        $teams = array_keys($results);
        
        foreach($teams as $teamName) {
            $team = $teamRepo->findOneBy(['shortName' => $teamName]);
            
            $results[$teamName]['firstGoal'] = $this->getFirstGoalForPlayerAndTeam($player, $team);
            $results[$teamName]['firstMatch'] = $this->getFirstMatchForPlayerAndTeam($player, $team);
            $results[$teamName]['fixtures'] = $this->getFixtureHistoryForPlayerAndTeam($player, $team);
        }
        
        $orderedResults = [];
        
        foreach($orderedTeams as $orderedTeam) {
            if (key_exists($orderedTeam->getShortName(), $results)) {
                $orderedResults[$orderedTeam->getShortName()] = $results[$orderedTeam->getShortName()];
            }
        }
        
        return $orderedResults;
    }
    
    private function getFirstGoalForPlayerAndTeam(Player $player, Team $team): ?string {
        $this->getEntityManager()->clear();
        $statsRepo = $this->getEntityManager()->getRepository(Stats::class);
                
        $firstGoal = $statsRepo->createQueryBuilder('s')
                      ->select('partial s.{ id }')
                      ->innerJoin('s.fixture', 'f')
                      ->addSelect('partial f.{ id, matchDate }')
                      ->innerJoin('f.opposition', 'o')
                      ->addSelect('partial o.{ id, name, logo }')
                      ->andWhere('s.player = :player')
                      ->andWhere('s.appearance = 1')
                      ->andWhere('f.matchDate IS NOT NULL')
                      ->andWhere('s.goals > 0')
                      ->andWhere('f.team = :team')
                      ->orderBy('f.matchDate', 'ASC')
                      ->setParameter('player', $player)
                      ->setParameter('team', $team)
                      ->setMaxResults(1)
                      ->getQuery()
                      ->getOneOrNullResult();
        
        return $firstGoal ? $firstGoal->getFixture()->getMatchDate()->format('d-m-Y') . ' vs ' . $firstGoal->getFixture()->getOpposition()->getName() : null;
    }
    
    private function getFirstMatchForPlayerAndTeam(Player $player, Team $team): ?string {
        $this->getEntityManager()->clear();
        $statsRepo = $this->getEntityManager()->getRepository(Stats::class);
                
        $firstMatch = $statsRepo->createQueryBuilder('s')
                      ->select('partial s.{ id }')
                      ->innerJoin('s.fixture', 'f')
                      ->addSelect('partial f.{ id, matchDate }')
                      ->innerJoin('f.opposition', 'o')
                      ->addSelect('partial o.{ id, name, logo }')
                      ->andWhere('s.player = :player')
                      ->andWhere('s.appearance = 1')
                      ->andWhere('f.matchDate IS NOT NULL')
                      ->andWhere('f.team = :team')
                      ->orderBy('f.matchDate', 'ASC')
                      ->setParameter('player', $player)
                      ->setParameter('team', $team)
                      ->setMaxResults(1)
                      ->getQuery()
                      ->getOneOrNullResult();
        
        return $firstMatch ? $firstMatch->getFixture()->getMatchDate()->format('d-m-Y') . ' vs ' . $firstMatch->getFixture()->getOpposition()->getName() : null;
    }
    
    private function getFixtureHistoryForPlayerAndTeam(Player $player, Team $team): array {
        $this->getEntityManager()->clear();
        $statsRepo = $this->getEntityManager()->getRepository(Stats::class);
        
        return $statsRepo->createQueryBuilder('s')
               ->addSelect('partial s.{ id, goals, bookings, appearance }')
               ->innerJoin('s.fixture', 'f')
               ->addSelect('partial f.{ id, matchDate, isHome, reportHomeScore, reportAwayScore }')
               ->innerJoin('f.competition', 'c')
               ->addSelect('partial c.{ id, name }')
               ->innerJoin('f.opposition', 'o')
               ->addSelect('partial o.{ id, name, logo }')
               ->innerJoin('f.team', 't')
               ->addSelect('partial t.{ id, name }')
               ->andWhere('s.player = :player')
               ->andWhere('s.appearance = 1')
               ->andWhere('f.matchDate IS NOT NULL')
               ->andWhere('f.team = :team')
               ->orderBy('f.matchDate', 'DESC')
               ->setParameter('player', $player)
               ->setParameter('team', $team)
               ->getQuery()
               ->execute();
        
    }
}
        
