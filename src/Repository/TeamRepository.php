<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Team;

class TeamRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Team::class);
    }

    /**
     * @return Team[]
     */
    public function findAllByPage($page, $count): array
    {
        $qb = $this->createQueryBuilder('t')
                   ->orderBy('t.id', 'ASC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(): int
    {
        return $this->count([]);
    }
}
        
