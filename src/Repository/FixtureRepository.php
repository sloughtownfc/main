<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\Fixture;
use App\Entity\Season;
use App\Entity\Team;

class FixtureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Fixture::class);
    }

    /**
     * @return Fixture[]
     */
    public function findAllByTeamAndPage(Team $team, $page, $count): array
    {
        $qb = $this->createQueryBuilder('f')
                   ->innerJoin('f.competition', 'c')
                   ->addSelect('c')
                   ->innerJoin('f.opposition', 'o')
                   ->where('f.team = :team')
                   ->setParameter('team', $team)
                   ->addSelect('o')
                   ->orderBy('f.matchDate', 'DESC')
                   ->setMaxResults($count);
        
        if ($page > 1) {
            $qb->setFirstResult(($page - 1) * $count);
        }
        
        return $qb->getQuery()->execute();
    }
    
    /**
     * @return Fixture[]
     */
    public function findAllByTeamAndSeason(Team $team, Season $season, $includeNonAttachedFixtures): array
    {       
        $qb = $this->createQueryBuilder('f')
                   ->innerJoin('f.competition', 'c')
                   ->addSelect('c')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->where('f.team = :team')
                   ->setParameter('team', $team)
                   ->setParameter('start', $season->getStartDate())
                   ->setParameter('end', $season->getEndDate())
                   ->orderBy('f.matchDate', 'ASC');
        
        if ($includeNonAttachedFixtures) {
            $qb->andWhere('f.matchDate BETWEEN :start AND :end OR f.matchDate IS NULL');
        }
        else {
            $qb->andWhere('f.matchDate BETWEEN :start AND :end');
        }
        
        return $qb->getQuery()->execute();
    }
    
    public function getCount(Team $team): int
    {
        return $this->count(['team' => $team]);
    }
    
    /**
     * @return Fixture
     */
    public function getFixtureAndEverythingById($id): ?Fixture
    {
        return $this->createQueryBuilder('f')
                   ->innerJoin('f.competition', 'c')
                   ->addSelect('c')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->leftJoin('f.reportMom', 'r')
                   ->addSelect('r')
                   ->leftJoin('f.stats', 's')
                   ->addSelect('s')
                   ->leftJoin('s.player', 'p')
                   ->addSelect('p')
                   ->andWhere('f.id = :id')
                   ->setParameter('id', $id)
                   ->getQuery()
                   ->getOneOrNullResult();
    }
    
    /**
     * @return Fixture
     */
    public function getFixtureAndCompetitionAndOppositionById($id): ?Fixture
    {
        return $this->createQueryBuilder('f')
                   ->innerJoin('f.competition', 'c')
                   ->addSelect('c')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->andWhere('f.id = :id')
                   ->setParameter('id', $id)
                   ->getQuery()
                   ->getOneOrNullResult();
    }
    
    /**
     * @return Fixture
     */
    public function getFixtureAndStatsAndPlayersById($id): ?Fixture
    {
        return $this->createQueryBuilder('f')
                   ->leftJoin('f.stats', 's')
                   ->addSelect('s')
                   ->leftJoin('s.player', 'p')
                   ->addSelect('p')
                   ->andWhere('f.id = :id')
                   ->setParameter('id', $id)
                   ->getQuery()
                   ->getOneOrNullResult();
    }
    
    /**
     * @return Fixture[]
     */
    public function getFixturesForDate(\DateTime $date): array
    {
        return $this->createQueryBuilder('f')
                    ->select('partial f.{ id, matchDate, isHome, reportHomeScore, reportAwayScore }')
                    ->innerJoin('f.opposition', 'o')
                    ->addSelect('partial o.{ id, name, logo }')
                    ->innerJoin('f.competition', 'c')
                    ->addSelect('partial c.{ id, name }')
                    ->andWhere('DAY(f.matchDate) = :day')
                    ->andWhere('MONTH(f.matchDate) = :month')
                    ->andWhere('YEAR(f.matchDate) < :year')
                    ->setParameter('day', $date->format('j'))
                    ->setParameter('month', $date->format('n'))
                    ->setParameter('year', $date->format('Y'))
                    ->addOrderBy('f.matchDate', 'DESC')
                    ->getQuery()
                    ->execute();
    }
    
    /**
     * @return Fixture
     */
    public function getNext(): ?Fixture
    {
        return $this->createQueryBuilder('f')
                   ->innerJoin('f.team', 't')
                   ->addSelect('t')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->andWhere('f.matchDate >= :date')
                   ->setParameter('date', (new \DateTime())->format('Y-m-d'))
                   ->addOrderBy('f.matchDate', 'ASC')
                   ->addOrderBy('t.id', 'ASC')
                   ->setMaxResults(1)
                   ->getQuery()
                   ->getOneOrNullResult();
    }
    
    /**
     * @return Fixture[]
     */
    public function listByTeamAndDirection(?Team $team = null, $direction = 'next', $count = 1): array
    {
        $qb = $this->createQueryBuilder('f')
                   ->innerJoin('f.team', 't')
                   ->addSelect('t')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->innerJoin('f.competition', 'c')
                   ->addSelect('c');

        if ($team) {
            $qb = $qb->andWhere('f.team = :team')
                     ->setParameter('team', $team);
        }
        
        if ($direction === 'next') {
            $qb = $qb->andWhere('f.matchDate >= :date')
                     ->setParameter('date', (new \DateTime())->format('Y-m-d'))
                     ->addOrderBy('f.matchDate', 'ASC');
        }
        else {
            $qb = $qb->andWhere('f.matchDate <= :date')
                     ->setParameter('date', (new \DateTime())->format('Y-m-d'))
                     ->addOrderBy('f.matchDate', 'DESC')
                     ->andWhere('f.reportHomeScore IS NOT NULL AND f.reportAwayScore IS NOT NULL');
        }
        
        return $qb->addOrderBy('t.id', 'ASC')
                  ->setMaxResults($count)
                  ->getQuery()
                  ->execute();
    }
    
    /**
     * @return Fixture
     */
    public function getUpcomingForTeam(Team $team): ?Fixture
    {
        return $this->createQueryBuilder('f')
                   ->innerJoin('f.team', 't')
                   ->addSelect('t')
                   ->innerJoin('f.opposition', 'o')
                   ->addSelect('o')
                   ->andWhere('f.matchDate >= :date')
                   ->andWhere('f.team = :team')
                   ->setParameter('date', (new \DateTime())->format('Y-m-d'))
                   ->setParameter('team', $team)
                   ->addOrderBy('f.matchDate', 'ASC')
                   ->addOrderBy('t.id', 'ASC')
                   ->setMaxResults(1)
                   ->getQuery()
                   ->getOneOrNullResult();
    }
    
    /**
     * @return Fixture
     */
    public function getRandomFixtureForDate(\DateTime $date): ?Fixture
    {
        return $this->createQueryBuilder('f')
                    ->select('partial f.{ id, matchDate, isHome, reportHomeScore, reportAwayScore }')
                    ->innerJoin('f.opposition', 'o')
                    ->addSelect('partial o.{ id, name,logo }')
                    ->innerJoin('f.competition', 'c')
                    ->addSelect('partial c.{ id, name }')
                    ->andWhere('DAY(f.matchDate) = :day')
                    ->andWhere('MONTH(f.matchDate) = :month')
                    ->andWhere('YEAR(f.matchDate) < :year')
                    ->setParameter('day', $date->format('j'))
                    ->setParameter('month', $date->format('n'))
                    ->setParameter('year', $date->format('Y'))
                    ->addOrderBy('RAND()', 'ASC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
    }
}
        
