<?php

namespace App\Service;

use GuzzleHttp\Client;

class RecaptchaService
{
    private $secret;
    
    public function __construct($recaptchaSecret)
    {
        $this->secret = $recaptchaSecret;
    }
    
    public function verify($recaptcha)
    {
        if (empty($recaptcha)) {
            return false;
        }
        
        $client = new Client();
        $response = $client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
            'form_params' => [
                'secret' => $this->secret,
                'response' => $recaptcha
            ]
        ]);
        
        if (($response->getStatusCode() != 200) || (!$responseData = json_decode($response->getBody()))) {
            return false;
        }
        
        return $responseData->success;
    }
}
