<?php

namespace App\Service;

use Twig\Environment;

class EmailerService
{
    private $twig;
    private $mailer;
    
    public function __construct(Environment $twig, \Swift_Mailer $mailer)
    {
        $this->twig = $twig;
        $this->mailer = $mailer;
    }
    
    public function sendAcademyEmail($to, $name, $returnEmail, $subject, $body)
    {
        $message = (new \Swift_Message('Message from STFC Academy Contact page'))
                    ->setTo($to)
                    ->setFrom('media@sloughtownfc.net')
                    ->setBody(
                        $this->twig->render('site/emails/academy-email.html.twig', [
                            'name' => $name,
                            'returnEmail' => $returnEmail,
                            'subject' => $subject,
                            'body' => $body
                        ]),
                        'text/html'
                    );
        
        $this->mailer->send($message);
    }
    
    public function sendContactEmail($to, $name, $returnEmail, $subject, $body)
    {
        $message = (new \Swift_Message('Message from STFC Contact page'))
                    ->setTo($to)
                    ->setFrom('media@sloughtownfc.net')
                    ->setBody(
                        $this->twig->render('site/emails/contact-email.html.twig', [
                            'name' => $name,
                            'returnEmail' => $returnEmail,
                            'subject' => $subject,
                            'body' => $body
                        ]),
                        'text/html'
                    );
        
        $this->mailer->send($message);
    }
    
    public function sendCommunityEmail($to, $name, $returnEmail, $subject, $body)
    {
        $message = (new \Swift_Message('Message from STFC Community page'))
                    ->setTo($to)
                    ->setFrom('media@sloughtownfc.net')
                    ->setBody(
                        $this->twig->render('site/emails/community-email.html.twig', [
                            'name' => $name,
                            'returnEmail' => $returnEmail,
                            'subject' => $subject,
                            'body' => $body
                        ]),
                        'text/html'
                    );
        
        $this->mailer->send($message);
    }
    
    public function sendJuniorsEmail($to, $name, $returnEmail, $subject, $body)
    {
        $message = (new \Swift_Message('Message from STFC Juniors page'))
                    ->setTo($to)
                    ->setFrom('media@sloughtownfc.net')
                    ->setBody(
                        $this->twig->render('site/emails/juniors-email.html.twig', [
                            'name' => $name,
                            'returnEmail' => $returnEmail,
                            'subject' => $subject,
                            'body' => $body
                        ]),
                        'text/html'
                    );
        
        $this->mailer->send($message);
    }
    
    public function sendLadiesEmail($to, $name, $returnEmail, $subject, $body)
    {
        $message = (new \Swift_Message('Message from STFC Ladies page'))
                    ->setTo($to)
                    ->setFrom('media@sloughtownfc.net')
                    ->setBody(
                        $this->twig->render('site/emails/ladies-email.html.twig', [
                            'name' => $name,
                            'returnEmail' => $returnEmail,
                            'subject' => $subject,
                            'body' => $body
                        ]),
                        'text/html'
                    );
        
        $this->mailer->send($message);
    }
}
