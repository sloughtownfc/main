<?php

namespace App\Service;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Entity\GalleryImage;

class ImageService
{
    private $galleryPath;
    private $imagine;
    
    public function __construct($galleryPath)
    {
        $this->galleryPath = $galleryPath;
        $this->imagine = new Imagine();
    }
    
    public function process(UploadedFile $file, string $type): GalleryImage
    {
        $md5 = md5_file($file->getRealPath());

        $ext = $file->getClientOriginalExtension();
        $path = join(DIRECTORY_SEPARATOR, [$this->galleryPath, $type]);
        $filename = "{$md5}.{$ext}";
        $thumbname = "{$md5}_s.{$ext}";
        
        $image = $this->imagine->open($file->getRealPath());
        $thumb = $this->generateThumb($image);

        $image->save($path . DIRECTORY_SEPARATOR . $filename);
        $thumb->save($path . DIRECTORY_SEPARATOR . $thumbname);
        
        unlink($file->getRealPath());
        
        return (new GalleryImage())
                ->setFilename($type . '/' . $filename)
                ->setThumbnail($type . '/' . $thumbname)
                ->setOriginalMd5($md5)
                ->setType($type);
    }
    
    private function generateThumb(ImageInterface $image): ImageInterface
    {
        $thumb = $image->thumbnail(new Box(75, 75));
        $size = $thumb->getSize();
        
        $height = 75 - $size->getHeight();
        $width = 75 - $size->getWidth();
        
        if ($height > 0) {
            $y = ceil($height / 2);
            
            $background = $this->imagine->create(new Box(75, 75));
            $thumb = $background->paste($thumb, new Point(0, $y));
        }
        else if ($width > 0) {
            $x = ceil($width / 2);
            
            $background = $this->imagine->create(new Box(75, 75));
            $thumb = $background->paste($thumb, new Point($x, 0));
        }
        
        return $thumb;
    }
}
