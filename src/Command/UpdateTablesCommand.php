<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\AttendanceEntry;
use App\Entity\LeagueEntry;
use App\Entity\Team;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class UpdateTablesCommand extends Command
{
    protected static $defaultName = 'app:update-tables';
    protected static $url = 'https://football-web-pages1.p.rapidapi.com/';
    
    private $apiKey;
    private $apiHost;
    private $em;
    private $output;
    
    public function __construct(EntityManagerInterface $em, $apiKey, $apiHost)
    {
        $this->em = $em;
        $this->apiKey = $apiKey;
        $this->apiHost = $apiHost;
        
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Loads the table XML data from footballwebpages.co.uk.')
             ->setName('app:update-tables');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $this->updateLeagues();
        $this->updateAttendance();
    }
    
    private function getJsonData($url)
    {
        $client = new Client();
        
        $response = $client->send(
            new Request('GET', $url, [
                'X-RapidAPI-Host' => $this->apiHost,
                'X-RapidAPI-Key' => $this->apiKey
            ])
        );

        return json_decode($response->getBody());
    }
    
    private function loadAttendanceData(): array
    {
        try
        {
            $url = UpdateTablesCommand::$url . 'attendances.json?comp=7';

            $jsonData = $this->getJsonData($url);
            $teams = $jsonData->attendances->teams;
            
            $data = [];

            foreach($teams as $team) {
                
                $data[] = [
                    'position' => $team->position,
                    'name' => $team->name,
                    'averageAttendance' => $team->{'average-attendance'} === 'n/a' ? 0 : $team->{'average-attendance'}
                ];
            }

            return $data;
        }
        catch (\Exception $e) {
            $this->output->writeln($e->getMessage());
            
            return [];
        }
    }
    
    private function loadLeagueData($type = null): array
    {
        try
        {
            $url = UpdateTablesCommand::$url . 'league-table.json?comp=7' . ($type != null ? "&sort=$type" : '');
            
            $jsonData = $this->getJsonData($url);
            $teams = $jsonData->{'league-table'}->teams;

            $data = [];

            $prop = 'all';
            switch ($type) {
                case 'home':
                    $prop = 'home';
                    break;
                case 'away':
                    $prop = 'away';
                    break;
                default:
                    break;
            }
            
            foreach($teams as $team) {
                $won = $team->{$prop.'-matches'}->won;
                $drawn = $team->{$prop.'-matches'}->drawn;
                
                $data[] = [
                    'position' => $team->position,
                    'name' => $team->name,
                    'played' => $team->{$prop.'-matches'}->played,
                    'won' => $won,
                    'drawn' => $drawn,
                    'lost' => $team->{$prop.'-matches'}->lost,
                    'for' => $team->{$prop.'-matches'}->for,
                    'against' => $team->{$prop.'-matches'}->against,
                    'points' => ($won * 3) + $drawn
                ];
            }

            return $data;
        }
        catch (\Exception $e) {
            $this->output->writeln($e->getMessage());
            
            return [];
        }
    }
    
    private function saveAttendanceData($data)
    {
        foreach($data as $entry) {
            $attendanceEntry = new AttendanceEntry();
            $attendanceEntry->setPosition($entry['position']);
            $attendanceEntry->setName($entry['name']);
            $attendanceEntry->setAverageAttendance($entry['averageAttendance']);

            $this->em->persist($attendanceEntry);
        }
        
        $this->em->flush();
    }
    
    private function saveLeagueData(Team $team, $data)
    {
        foreach($data as $key => $type) {
            foreach($type as $entry) {
                $leagueEntry = new LeagueEntry();
                $leagueEntry->setTeam($team);
                $leagueEntry->setType($key);
                $leagueEntry->setPosition($entry['position']);
                $leagueEntry->setName($entry['name']);
                $leagueEntry->setPlayed($entry['played']);
                $leagueEntry->setWon($entry['won']);
                $leagueEntry->setDrawn($entry['drawn']);
                $leagueEntry->setLost($entry['lost']);
                $leagueEntry->setFor($entry['for']);
                $leagueEntry->setAgainst($entry['against']);
                $leagueEntry->setPoints($entry['points']);
                
                $this->em->persist($leagueEntry);
            }
        }
        
        $this->em->flush();
    }
    
    private function clearLeagueEntries(Team $team)
    {
        $q = $this->em->createQuery('DELETE FROM App\Entity\LeagueEntry le WHERE le.team = :team');
        
        $q->setParameter('team', $team)
          ->execute();
        
        $this->em->flush();
    }
    
    private function truncateTable($name)
    {
        $connection = $this->em->getConnection();
        $connection->executeUpdate($connection->getDatabasePlatform()->getTruncateTableSQL($name, false));
    }
    
    private function updateAttendance()
    {
        $data = $this->loadAttendanceData();
        
        if (count($data) == 0) {
            $this->output->writeln('Missing attendance data.');
            
            return;
        }
        
        $this->truncateTable('attendance_entries');
        $this->saveAttendanceData($data);
    }    
    
    private function updateLeagues()
    {
        $data = [];
        $data['full'] = $this->loadLeagueData();
        $data['home'] = $this->loadLeagueData('home');
        $data['away'] = $this->loadLeagueData('away');
        $data['goals'] = $this->loadLeagueData('scored');
        $data['conceded'] = $this->loadLeagueData('conceded');
        
        if (count($data['full']) == 0 || count($data['home']) == 0 || count($data['away']) == 0 || count($data['goals']) == 0 || count($data['conceded']) == 0) {
            $this->output->writeln('Missing league data.');
            
            return;
        }
        
        $team = $this->em->getRepository(Team::class)->find(1);
        
        $this->clearLeagueEntries($team);
        $this->saveLeagueData($team, $data);
    }
}
