<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\LeagueEntry;
use App\Entity\Team;

class UpdateFATablesCommand extends Command
{
    protected static $defaultName = 'app:update-fa-tables';
    protected static $eds = 'https://fulltime.thefa.com/table.html?league=209426463&selectedSeason=424806740&selectedDivision=681316394&selectedCompetition=0&selectedFixtureGroupKey=1_961733172%20#tab-2';
    protected static $ladies = 'https://fulltime.thefa.com/table.html?league=479698&selectedSeason=362016689&selectedDivision=523583784&selectedCompetition=0&selectedFixtureGroupKey=1_547602386#tab-2';
    
    private $em;
    private $output;
    
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Loads the table XML data from thefa.com')
             ->setName('app:update-fa-tables');
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        
        $this->updateLeagues(UpdateFATablesCommand::$ladies, 2);
        $this->updateLeagues(UpdateFATablesCommand::$eds, 3);
    }
    
    private function getXmlXPath($url): \DOMXPath
    {
        $doc = new \DOMDocument;
        @$doc->loadHtml(file_get_contents($url));
        
        return new \DOMXPath($doc);
    }

    private function loadLeagueData($url): array
    {
        try
        {           
            $xpath = $this->getXmlXPath($url);
            $teams = $xpath->query("/html/body//section[@id='league-table']//div[contains(@class, 'league-standings-table')]//table//tbody//tr");
            
            $data = [];

            foreach($teams as $team) {
                $nodes = $xpath->query("td", $team);

                /*
                0: Pos
                1: Team
                2: P
                3: H - W
                4: H - D
                5: H - L
                6: H - F
                7: H - A
                8: A - W
                9: A - D
                10: A - L
                11: A - F
                12: A - A
                13: O - W
                14: O - D
                15: O - L
                16: O - F
                17: O - A
                18: GD
                19: PTS
                */

                $pos = trim($nodes->item(0)->nodeValue);
                $name = trim($nodes->item(1)->nodeValue);
                $played = trim($nodes->item(2)->nodeValue);
                
                $won = trim($nodes->item(13)->nodeValue);
                $drawn = trim($nodes->item(14)->nodeValue);
                $lost = trim($nodes->item(15)->nodeValue);
                $for = trim($nodes->item(16)->nodeValue);
                $against = trim($nodes->item(17)->nodeValue);
                $points = trim($nodes->item(19)->nodeValue, "\n\r\t\v\x00*");
                        
                $data['full'][] = [
                  'position' => $pos,
                  'name' => $name,
                  'played' => $played,
                  'won' => $won,
                  'drawn' => $drawn,
                  'lost' => $lost,
                  'for' => $for,
                  'against' => $against,
                  'points' => $points
                ];
                
                $home_won = trim($nodes->item(3)->nodeValue);
                $home_drawn = trim($nodes->item(4)->nodeValue);
                $home_lost = trim($nodes->item(5)->nodeValue);
                $home_for = trim($nodes->item(6)->nodeValue);
                $home_away = trim($nodes->item(7)->nodeValue);
                
                $data['home'][] = [
                  'name' => $name,
                  'played' => $home_won + $home_drawn + $home_lost,
                  'won' => $home_won,
                  'drawn' => $home_drawn,
                  'lost' => $home_lost,
                  'for' => $home_for,
                  'against' => $home_away,
                  'gd' => $home_for - $home_away,
                  'points' => ($home_won * 3) + ($home_drawn * 1)
                ];
                
                $away_won = trim($nodes->item(8)->nodeValue);
                $away_drawn = trim($nodes->item(9)->nodeValue);
                $away_lost = trim($nodes->item(10)->nodeValue);
                $away_for = trim($nodes->item(11)->nodeValue);
                $away_against = trim($nodes->item(12)->nodeValue);
                
                $data['away'][] = [
                  'name' => $name,
                  'played' => $away_won + $away_drawn + $away_lost,
                  'won' => $away_won,
                  'drawn' => $away_drawn,
                  'lost' => $away_lost,
                  'for' => $away_for,
                  'against' => $away_against,
                  'gd' => $away_for - $away_against,
                  'points' => ($away_won * 3) + ($away_drawn * 1)
                ];
                
                $data['goals'][] = [
                  'name' => $name,
                  'played' => $played,
                  'won' => $won,
                  'drawn' => $drawn,
                  'lost' => $lost,
                  'for' => $for,
                  'against' => $against,
                  'points' => $points
                ];
                
                $data['conceded'][] = [
                  'name' => $name,
                  'played' => $played,
                  'won' => $won,
                  'drawn' => $drawn,
                  'lost' => $lost,
                  'for' => $for,
                  'against' => $against,
                  'points' => $points
                ];
            }

            usort($data['home'], $this->sortBy(['points', 'gd', 'for']));
            usort($data['away'], $this->sortBy(['points', 'gd', 'for']));
            usort($data['goals'], $this->sortBy(['for']));
            usort($data['conceded'], $this->sortBy(['against']));
            
            return $data;
        }
        catch (\Exception $e) {
            $this->output->writeln($e->getLine() . '::' . $e->getMessage());
            
            return [];
        }
    }
    
    private function saveLeagueData(Team $team, $data)
    {        
        foreach($data as $key => $type) {
            $i = 1;
            
            foreach($type as $entry) {
                $leagueEntry = new LeagueEntry();
                $leagueEntry->setTeam($team);
                $leagueEntry->setType($key);
                $leagueEntry->setPosition($i);
                $leagueEntry->setName($entry['name']);
                $leagueEntry->setPlayed($entry['played']);
                $leagueEntry->setWon($entry['won']);
                $leagueEntry->setDrawn($entry['drawn']);
                $leagueEntry->setLost($entry['lost']);
                $leagueEntry->setFor($entry['for']);
                $leagueEntry->setAgainst($entry['against']);
                $leagueEntry->setPoints($entry['points']);
                
                $this->em->persist($leagueEntry);
                
                $i++;
            }
        }
        
        $this->em->flush();
    }
    
    private function clearLeagueEntries(Team $team)
    {
        $q = $this->em->createQuery('DELETE FROM App\Entity\LeagueEntry le WHERE le.team = :team');
        
        $q->setParameter('team', $team)
          ->execute();
        
        $this->em->flush();
    }
    
    private function updateLeagues($faId, $teamId)
    {
        $data = $this->loadLeagueData($faId);
        
        if (count($data['full']) == 0 || count($data['home']) == 0 || count($data['away']) == 0 || count($data['goals']) == 0 || count($data['conceded']) == 0) {
            $this->output->writeln('Missing league data.');
            
            return;
        }
        
        $team = $this->em->getRepository(Team::class)->find($teamId);
        
        $this->clearLeagueEntries($team);
        $this->saveLeagueData($team, $data);
    }
    
    private function sortBy($keys)
    {
        return function($a, $b) use ($keys)
        {
            for($i = 0; $i < count($keys); $i++) {
                if ($a[$keys[$i]] === $b[$keys[$i]]) {
                    continue;
                }
                
                return $a[$keys[$i]] > $b[$keys[$i]] ? -1 : 1;
            }
            
            return strnatcmp(substr(strtolower($a['name']), 0, 3), substr(strtolower($b['name']), 0, 3));
        };
    }
}