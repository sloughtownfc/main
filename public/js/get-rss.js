$(document).ready(function () {
    $.ajax({
        url: '/external-blog/sloughtownsoapbox',
        type: 'GET',
        dataType: "xml",
        success: function(xml) {
            var item = $("item:first", xml);

            var itemURL = item.find("link");
            var blogURL = "<a href='" + itemURL.text() + "' class='btn btn-link external' target='_blank'><i class='fas fa-external-link-alt'></i>Read more</a>";

            var blogTitle = "<h4>" + item.find("title").text() + "</h4>";

            var itemDesc = $(item.find("description").text());
            itemDesc.find("span:first").remove();
            var descText = itemDesc.text().replace(/(<([^>]+)>)/ig,"").substr(0, 200).trim();
            var blogDesc = "<p>" + descText + "...</p>";
            
            $("#soapbox_content").append(blogTitle);
            $("#soapbox_content").append(blogDesc);
            $("#soapbox_content").append(blogURL);
        },
        error: function() {
            $("#soapbox_content").hide();
        }
    });
});