$(document).ready(function() {
    $('button.btn-image-gallery').on('click', function () {
        $container = $('<div id="image-gallery-container" class="modal fade" tabindex="-1" style="display: none;"></div>');
        $('body').append($container);

        var baseUrl = $(this).data('url');

        $container.data('target-id', $(this).data('target-id'));
        $container.load(baseUrl, function() {
          $container.modal({});
        });

        $container.on('keypress', 'input.input-search', function(e) {
            if (e.which === 13) {
                $('button.btn-search', $container).click();
                return false;
            }
        });

        $container.on('click', 'button.btn-search', function() {
            $('button.btn-primary', $container).prop('disabled', true);
            $container.load(baseUrl + '?tag=' + encodeURI($('input', $(this).parent()).val()));
        });

        $container.on('click', 'button.btn-nav', function() {
            $('button.btn-primary', $container).prop('disabled', true);
            $container.load($(this).data('url'));
        });

        $container.on('click', 'div.selectable', function() {
            $('div.selectable', $container).removeClass('selected border-primary');
            $(this).addClass('selected border-primary');
            $('button.btn-select', $container).prop('disabled', false);
        });

        $container.on('click', 'button.btn-select', function() {
            $('input#' + $container.data('target-id')).val($('div.selected img', $container).data('image'));
            $container.modal('hide');
        });

        $container.on('hidden.bs.modal', function() {
            $(this).removeData('target-id');
            $(this).modal('dispose');
            $(this).remove();
        });
    });
});


