$(document).ready(function () {
    function getTableFor(event) {
    $.get("/api/tables/league?teamUrl=" + event.data.team+ "&type=full&count=3", function(data) {
        var table = '';
        data.table.forEach((row) => {
            if (row.name === data.team.tableName) {
                var tableRow = "<tr class=\"slough\"><th scope=\"row\" class=\"text-center\">"+ row.position +"</th><td>"+ row.name +"</td><td class=\"text-center\">"+ row.played +"</td><td class=\"text-center\">"+ row.won +"</td><td class=\"text-center\">"+ row.drawn +"</td><td class=\"text-center\">"+ row.lost +"</td><td class=\"text-center d-none d-sm-table-cell\">"+ row.for +"</td><td class=\"text-center d-none d-sm-table-cell\">"+ row.against +"</td><td class=\"text-center\">"+ (row.goalDifference ?? 0) +"</td><td class=\"text-center\">"+ row.points +"</td>";
                table += tableRow;
            } else {
                var tableRow = "<tr><th scope=\"row\" class=\"text-center\">"+ row.position +"</th><td>"+ row.name +"</td><td class=\"text-center\">"+ row.played +"</td><td class=\"text-center\">"+ row.won +"</td><td class=\"text-center\">"+ row.drawn +"</td><td class=\"text-center\">"+ row.lost +"</td><td class=\"text-center d-none d-sm-table-cell\">"+ row.for +"</td><td class=\"text-center d-none d-sm-table-cell\">"+ row.against +"</td><td class=\"text-center\">"+ (row.goalDifference ?? 0) +"</td><td class=\"text-center\">"+ row.points +"</td>";
                table += tableRow;
            }
            $("#homepage-table .leagueTable tbody").html(table);

            if (event.target !== undefined) {
               $("a", $(event.target).closest("nav")).removeClass("active");
               $(event.target).addClass("active");
            }
        });
    }).fail(function() {
        $("#homepage-table").hide();
    });
    }

    function getNextFixtureFor(event) {
        $.get("/api/fixtures?teamUrl=" + event.data.team+ "&count=1&direction=next", function(data) {
            if (data.fixtures.length > 0) {
                $("#next-fixture").show();
                $("#next-fixture .card").removeClass("away");

                var fixture = data.fixtures[0];
            
                var nextFixture = '';

                var venue = fixture.isHome ? 'Arbour Park' : fixture.opposition.groundsVenue;
                var location = fixture.isHome ? 'Home' : 'Away';

                if (fixture.isHome == false) {
                    $("#next-fixture .card").addClass("away");
                }

                nextFixture = "<div class=\"card-header\"><div class=\"row align-items-center\"><div class=\"col\"><time class=\"date mr-2\" datetime=\""+ fixture.matchDate +"\">"+ fixture.matchDate +"</time><time class=\"kickoff\" datetime=\""+ fixture.kickoff +"\">"+ fixture.kickoff +"</time></div><div class=\"col text-right\"><span>"+ fixture.competition.name +" - </span> <span>"+ location +"</span> </div></div></div><div class=\"card-body\"><div class=\"row align-items-center\"><div class=\"col-3\"><img width=\"65\" class=\"lazyload\" data-src=\""+ fixture.opposition.logo +"\" alt=\""+ fixture.opposition.name +"\"></div><div class=\"col-9 col-sm-6 fixture-opposition\"><span>"+ fixture.opposition.name +"</span><address>"+ venue +"</address>";

                if (fixture.matchSponsor !== null || fixture.ballSponsor  !== null ) {
                    var sponsor ="<div class=\"row pt-3\"><div class=\"col-12\">";
                    nextFixture += sponsor;

                    if (fixture.matchSponsor !== null) {
                        var matchSponsor = "Sponsored by "+ fixture.matchSponsor +"<br>";
                        nextFixture += matchSponsor;
                    }
                    if (fixture.ballSponsor !== null) {
                        var ballSponsor = "Ball sponsored by "+ fixture.ballSponsor +"";
                        nextFixture += ballSponsor;
                    }
                    var endSponsor = "</div></div>";
                    nextFixture += endSponsor;
                }
        
                nextFixture += "</div></div></div>";

                if (fixture.ticketUrl !== null || fixture.liveStreamUrl  !== null ) {
                    var urls = "<div class=\"card-footer text-center\"><div class=\"row\"><div class=\"col-12\">";
                    nextFixture += urls;

                    if (fixture.ticketUrl !== null) {
                        var ticketUrl = "<a class=\"btn btn-link mt-2\" href=\""+ fixture.ticketUrl +"\" role=\"button\" target=\"_blank\"><i class=\"fas fa-ticket-alt\"></i> Buy Tickets</a>";
                        nextFixture += ticketUrl;
                    }

                    if (fixture.liveStreamUrl !== null) {
                        var liveStreamUrl = "<a class=\"btn btn-link mt-2 ml-2\" href=\""+ fixture.liveStreamUrl +"\" role=\"button\" target=\"_blank\"><i class=\"fas fa-tv\"></i> Stream Live</a>";
                        nextFixture += liveStreamUrl;

                    }
                    var endUrls = "</div></div></div>";
                    nextFixture += endUrls;
                }

            $("#next-fixture .card").html(nextFixture);
        } else {
            $("#next-fixture .card").removeClass("away");
            
            nextFixture = "<div class=\"card-header\"><div class=\"row align-items-center\"><div class=\"col\"></div></div></div><div class=\"card-body\"><div class=\"row align-items-center\"><div class=\"col\"><p>New fixtures announced soon.</p></div></div></div>";

            $("#next-fixture .card").html(nextFixture);
        }
        }).fail(function() {
            $("#next-fixture").hide();
        });
    }

    function getPrevFixtureFor(event) {
        $.get("/api/fixtures?teamUrl=" + event.data.team+ "&count=1&direction=prev", function(data) {
            if (data.fixtures.length > 0) {
                $("#last-fixture").show();
                $("#last-fixture .card").removeClass("away");

                var fixture = data.fixtures[0];

                var prevFixture = '';

                var location = fixture.isHome ? 'Home' : 'Away';
                var homeTeam = fixture.isHome ? 'Slough Town' : fixture.opposition.name;
                var awayTeam = fixture.isHome ? fixture.opposition.name : 'Slough Town';

                if (fixture.isHome == false) {
                    $("#last-fixture .card").addClass("away");
                }

                prevFixture = "<div class=\"card-header\"><div class=\"row align-items-center\"><div class=\"col\"><time class=\"date\" datetime=\""+ fixture.matchDate +"\">"+ fixture.matchDate +"</time></div><div class=\"col text-right\"><span>"+ fixture.competition.name +" - </span><span>"+ location +"</span></div></div></div><div class=\"card-body\"><div class=\"row align-items-center\"><div class=\"col-3\"><img width=\"65\" class=\"lazyload\" data-src=\""+ fixture.opposition.logo +"\" alt=\""+ fixture.opposition.name +"\"></div><div class=\"col-9 col-sm-6 fixture-opposition\"><span class=\"score\">"+ fixture.reportHomeScore +"</span> <span>"+ homeTeam +"</span><br><span class=\"score\">"+ fixture.reportAwayScore +"</span> <span>"+ awayTeam +"</span></div></div></div>";

                if (fixture.reportPublished !== false || fixture.videoPublished  !== false ) {
                    var urls = "<div class=\"card-footer text-center\"><div class=\"row\"><div class=\"col-12\">";
                    prevFixture += urls;

                    if (fixture.reportPublished !== false) {
                        var reportUrl = "<a class=\"btn btn-link mt-2\" href=\""+ fixture.reportUrl +"\" role=\"button\"><i class=\"fas fa-file\"></i> Match Report</a>";
                        prevFixture += reportUrl;
                    }

                    if (fixture.videoPublished !== false) {
                        var videoUrl = "<a class=\"btn btn-link mt-2 ml-2\" href=\""+ fixture.videoUrl +"\" role=\"button\"><i class=\"fas fa-play-circle\"></i> Highlights</a>";
                    }

                    var endUrls = "</div></div></div>";
                    prevFixture += endUrls;
                }

                $("#last-fixture .card").html(prevFixture);
             } else {
            $("#last-fixture").hide();
        }
        }).fail(function() {
            $("#last-fixture").hide();
        });
    }


    function getTablesFor(event) {
        getTableFor(event);
        getNextFixtureFor(event);
        getPrevFixtureFor(event);
    }

    getTableFor({ data: { team: 'mens' }});
    getNextFixtureFor({ data: { team: 'mens' }});
    getPrevFixtureFor({ data: { team: 'mens' }});

    $("a.mens").on('click', { team: "mens" }, getTablesFor);
    $("a.ladies").on('click', { team: "ladies" }, getTablesFor);
    $("a.eds").on('click', { team: "elite-development-squad" }, getTablesFor);
});